﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WorldMiniGamesV1.Services;
using WorldMiniGamesV1.View;
using Xamarin.Forms;

namespace WorldMiniGamesV1.ViewModel
{
    public class MainPageViewModel : ContentPage, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public INavigation Navigation { get; set; }
        public ICommand CJugar { get; set; }
        public ICommand CRank { get; set; }
        public ICommand CSalir { get; set; }

        public ICommand TClickerPage { get; set; }
        public ICommand TNinjasPage { get; set; }
        public ICommand TEgyptPage { get; set; }
        public ICommand TNasPage { get; set; }
        public ICommand TToposPage { get; set; }
        public ICommand TSumaPage { get; set; }
        public ICommand TVasosPage {get; set;}
        public ICommand TTicToePage { get; set; }

        public ICommand ClickerPage { get; set; }
        public ICommand NinjasPage { get; set; }
        public ICommand EgyptPage { get; set; }
        public ICommand NasPage { get; set; }
        public ICommand ToposPage { get; set; }
        public ICommand SumaPage { get; set; }
        public ICommand VasosPage { get; set; }
        public ICommand TicToePage { get; set; }

        public ICommand CerrarAppFinal { get; set; }

        public static bool enableTopos = true;
        public static bool enableEgypt = true;
        public static bool enableNAS = true;
        public static bool enableClicker = true;
        public static bool enableNinjas = true;
        public static bool enableSuma = true;
        public static bool enableVasos = true;
        public static bool enableTicToe = true;

        public static double opacityTopos = 1;
        public static double opacityEgypt = 1;
        public static double opacityNAS = 1;
        public static double opacityClicker = 1;
        public static double opacityNinjas = 1;
        public static double opacitySuma = 1;
        public static double opacityVasos = 1;
        public static double opacityTicToe = 1;

        public static int playedGames = 0;
        public static int puntuacionTotal = 0;

        public static int bugEgypt = 0;
        public static int bugSuma = 0;
        public static int bugTicToc = 0;

        public String imagenPopup = "Menu_img/mundo.png";
        public String txtPuntos;

        public String nombreJugador;
        public static int puntuacion;
        public int Puntuacion
        {
            set
            {
                if (puntuacion != value)
                {
                    puntuacion = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Puntuacion"));
                    }
                }
            }
            get
            {
                return puntuacion;
            }
        }
        public double OpacityTopos
        {
            set
            {
                if (opacityTopos != value)
                {
                    opacityTopos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacityTopos"));
                    }
                }
            }
            get
            {
                return opacityTopos;
            }
        }
        public double OpacityEgypt
        {
            set
            {
                if (opacityEgypt != value)
                {
                    opacityEgypt = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacityEgypt"));
                    }
                }
            }
            get
            {
                return opacityEgypt;
            }
        }
        public double OpacityNAS
        {
            set
            {
                if (opacityNAS != value)
                {
                    opacityNAS = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacityNAS"));
                    }
                }
            }
            get
            {
                return opacityNAS;
            }
        }
        public double OpacityClicker
        {
            set
            {
                if (opacityClicker != value)
                {
                    opacityClicker = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacityClicker"));
                    }
                }
            }
            get
            {
                return opacityClicker;
            }
        }
        public double OpacityNinjas
        {
            set
            {
                if (opacityNinjas != value)
                {
                    opacityNinjas = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacityNinjas"));
                    }
                }
            }
            get
            {
                return opacityNinjas;
            }
        }
        public double OpacitySuma
        {
            set
            {
                if (opacitySuma != value)
                {
                    opacitySuma = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacitySuma"));
                    }
                }
            }
            get
            {
                return opacitySuma;
            }
        }
        public double OpacityVasos
        {
            set
            {
                if (opacityVasos != value)
                {
                    opacityVasos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacityVasos"));
                    }
                }
            }
            get
            {
                return opacityVasos;
            }
        }
        public double OpacityTicToe

        {
            set
            {
                if (opacityTicToe != value)
                {
                    opacityTicToe = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("OpacityTicToe"));
                    }
                }
            }
            get
            {
                return opacityTicToe;
            }
        }
        public int PlayedGames
        {
            set
            {
                if (playedGames != value)
                {
                    playedGames = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("PlayedGames"));
                    }
                }
            }
            get
            {
                return playedGames;
            }
        }
        public String ImagenPopup
        {
            set
            {
                if (imagenPopup != value)
                {
                    imagenPopup = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup"));
                    }
                }
            }
            get
            {
                return imagenPopup;
            }
        }
        public String TxtPuntos
        {
            set
            {
                if (txtPuntos != value)
                {
                    txtPuntos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TxtPuntos"));
                    }
                }
            }
            get
            {
                return txtPuntos;
            }
        }
        public String NombreJugador
        {
            set
            {
                if (nombreJugador != value)
                {
                    nombreJugador = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("NombreJugador"));
                    }
                }
            }
            get
            {
                return nombreJugador;
            }
        }

        //public MainPageViewModel()
        //{
        //    CJugar = new Command(ClickJugar);
        //    CSalir = new Command(ClickRank);
        //    CSalir = new Command(ClickSalir);
        //}


        public MainPageViewModel(String txtPuntos, INavigation navigation)
        {

            this.TxtPuntos = txtPuntos;
            Navigation = navigation;
            CJugar = new Command(ClickJugar);
            CSalir = new Command(ClickRank);
            CSalir = new Command(ClickSalir);

            TClickerPage = new Command(TSpeedClicker);
            TNinjasPage = new Command(TNinjas);
            TEgyptPage = new Command(TEgypt);
            TNasPage = new Command(TNas);
            TToposPage = new Command(TTopos);
            TSumaPage = new Command(TSuma);
            TVasosPage = new Command(TVasos);
            TTicToePage = new Command(TTicToe);

            ClickerPage = new Command(SpeedClicker);
            NinjasPage = new Command(Ninjas);
            EgyptPage = new Command(Egypt);
            NasPage = new Command(Nas);
            ToposPage = new Command(Topos);
            SumaPage = new Command(Suma);
            VasosPage = new Command(Vasos);
            TicToePage = new Command(TicToe);


            CerrarAppFinal = new Command(EndGame);
            Puntuacion = puntuacionTotal;

            //EnableClicker = true;
            //EnableSuma = true;
            //EnableTopos = true;
            //EnableNinjas = true;
            //EnableEgypt = true;
            //EnableNinjas = true;
        }

        // Botones principales
        private async void ClickJugar()
        {
            //musicaAmbiente();
            await Navigation.PushModalAsync(new SumaPage());
        }
        private void ClickRank()
        {

        }
        private void ClickSalir()
        {

        }

        // Empezar Tutorial Juego
        private async void TSpeedClicker()
        {

            if (enableClicker == true)
            {
                OpacityClicker = 0.5;
                enableClicker = false;
                await Navigation.PushModalAsync(new TutorialClicker());
            }
        }
        private async void TNinjas()
        {
            if (enableNinjas == true)
            {
                OpacityNinjas = 0.5;
                enableNinjas = false;
                await Navigation.PushModalAsync(new TutorialNinjas());
            }
        }
        private async void TEgypt()
        {
            if (enableEgypt == true)
            {
                OpacityEgypt = 0.5;
                enableEgypt = false;
                await Navigation.PushModalAsync(new TutorialEgypt());
            }
        }
        private async void TNas()
        {
            if (enableNAS == true)
            {
                OpacityNAS = 0.5;
                enableNAS = false;
                await Navigation.PushModalAsync(new TutorialNAS());
            }
        }
        private async void TTopos()
        {
            if (enableTopos == true)
            {
                OpacityTopos = 0.5;
                enableTopos = false;
                await Navigation.PushModalAsync(new TutorialTopos());
            }
        }
        private async void TSuma()
        {
            if (enableSuma == true)
            {
                OpacitySuma = 0.5;
                enableSuma = false;
                await Navigation.PushModalAsync(new TutorialSuma());
            }
        }

        private async void TVasos()
        {
            if (enableVasos == true)
            {
                OpacityVasos = 0.5;
                enableVasos = false;
                await Navigation.PushModalAsync(new TutorialVasos());
            }
        }

        private async void TTicToe()
        {
            if (enableTicToe == true)
            {
                OpacityTicToe = 0.5;
                enableTicToe = false;
                await Navigation.PushModalAsync(new TutorialTicToe());
            }
        }

        //Empezar Juego
        private async void SpeedClicker()
        {
            await Navigation.PushModalAsync(new ClickerPage());
        }
        private async void Ninjas()
        {
            await Navigation.PushModalAsync(new NinjasPage2());
        }
        private async void Egypt()
        {
            await Navigation.PushModalAsync(new EgyptPage());
        }
        private async void Nas()
        {
            await Navigation.PushModalAsync(new NasPage());
        }
        private async void Topos()
        {
            await Navigation.PushModalAsync(new ToposPage());
        }
        private async void Suma()
        {
            await Navigation.PushModalAsync(new SumaPage());
        }
        private async void Vasos()
        {
            await Navigation.PushModalAsync(new VasosPage());
        }
        private async void TicToe()
        {
            await Navigation.PushModalAsync(new TicToePage());
        }


        public async void PopUp()
        {
            TxtPuntos = "PUNTUACIÓN: " + puntuacionTotal + " PUNTOS";
            var pr = new PopUpFinal(TxtPuntos, Navigation);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left

            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);

        }

        public void Basededatos()
        {
            Controlador controlador = new Controlador();
            NombreJugador = NombreJugador;

            List<Jugador> listaControlador = new List<Jugador>();

            listaControlador = Controlador.ObtenerJugadores();

            controlador.Nombres = NombreJugador;
            controlador.Puntuaciones = puntuacionTotal;

            Controlador.AgregarJugador(NombreJugador, puntuacionTotal);
        }

        public async void EndGame()
        {
            enableTopos = true;
            enableEgypt = true;
            enableNAS = true;
            enableClicker = true;
            enableNinjas = true;
            enableSuma = true;
            enableVasos = true;
            enableTicToe = true;

            opacityTopos = 1;
            opacityEgypt = 1;
            opacityNAS = 1;
            opacityClicker = 1;
            opacityNinjas = 1;
            opacitySuma = 1;
            opacityVasos = 1;
            opacityTicToe = 1;

            playedGames = 0;
            Basededatos();
            puntuacionTotal = 0;

            PopupNavigation.Instance.PopAsync();
            await Navigation.PushModalAsync(new MainPage());
           


        }






        //public async Task musicaAmbiente()
        //{
        //    var assembly1 = typeof(App).GetTypeInfo().Assembly;
        //    Stream audioStream1 = assembly1.GetManifestResourceStream("WorldMiniGamesV1.Sounds.musica1.mp3");
        //    var audio1 = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
        //    audio1.Load(audioStream1);
        //    audio1.Play();
        //}


    }
}