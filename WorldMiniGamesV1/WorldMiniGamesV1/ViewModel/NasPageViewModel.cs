﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System.IO;
using System.Reflection;
using WorldMiniGamesV1.View;
using System.Collections;
using WorldMiniGamesV1.ViewModel;

namespace WorldMiniGamesV1
{
    public class NasPageViewModel : INotifyPropertyChanged
    {
        
        public event PropertyChangedEventHandler PropertyChanged;

        public INavigation Navigation { get; set; }

        //Binding Comandos
        public ICommand J5_Btn_azul { get; set; }
        public ICommand J5_Btn_blanco { get; set; }
        public ICommand J5_Btn_rojo { get; set; }
        public ICommand J5_NewPage { get; set; }
        public ICommand J5_Start { get; set; }        
        public ICommand CerrarApp { get; set; }

        //Variables (int) Propiedad Labels
        public int j5_Contador;
        public int j5_Cronometro;

        //Variables (String) Propiedad Labels y Imagen 
        public String j5_Question1;
        public String j5_Question2;
        public String j5_ProgressBar = "J5_img/barra00.png";
        public String j5_TxtPuntos;
        public String imagenPopup = "J5_img/frances.png";

        //Variables (Color) Propiedad TextColor
        public Color j5_ColorPregunta;
        public Color j5_ColorCronometro;

        //Variables (int) Propiedad TransitionX
        public int j5_PosBtnAzul;
        public int j5_PosBtnBlanco;
        public int j5_PosBtnRojo;

        //Variables (bool) Propiedad isVisible
        public bool j5_VisibleStart = true;
        public bool j5_Vitem1;
        public bool j5_Vitem2;
        public bool j5_Vitem3;
        public bool j5_Vitem4;
        public bool j5_Vitem5;

        //Constructor MainPageViewModel
        public NasPageViewModel()
        {

            J5_Start = new Command(J5_StartGame);
            J5_Btn_azul = new Command(J5_clickAzul);
            J5_Btn_blanco = new Command(J5_clickBlanco);
            J5_Btn_rojo = new Command(J5_clickRojo);
            CerrarApp = new Command(J5_EndGame);

            J5_Contador = 0;
            J5_Cronometro = 10;

            J5_PosBtnAzul = 0;
            J5_PosBtnBlanco = 0;
            J5_PosBtnRojo = 0;

        }

        //Constructor MainPageViewModel con Parametro txtPuntos
        public NasPageViewModel(String j5_TxtPuntos, INavigation navigation)
        {
            this.TxtPuntos = j5_TxtPuntos;
            this.Navigation = navigation;

            J5_Start = new Command(J5_StartGame);
            J5_Btn_azul = new Command(J5_clickAzul);
            J5_Btn_blanco = new Command(J5_clickBlanco);
            J5_Btn_rojo = new Command(J5_clickRojo);
            CerrarApp = new Command(J5_EndGame);

            J5_Contador = 0;
            J5_Cronometro = 30;

            J5_PosBtnAzul = 0;
            J5_PosBtnBlanco = 0;
            J5_PosBtnRojo = 0;

        }

        //Getters y Setters Binding Atributos
        public int J5_Contador
        {
            set
            {
                if (j5_Contador != value)
                {
                    j5_Contador = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Contador"));
                    }
                }
            }
            get
            {
                return j5_Contador;
            }
        }
        public int J5_Cronometro
        {
            set
            {
                if (j5_Cronometro != value)
                {
                    j5_Cronometro = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Cronometro"));
                    }
                }
            }
            get
            {
                return j5_Cronometro;
            }
        }

        public String J5_Question1
        {
            set
            {
                if (j5_Question1 != value)
                {
                    j5_Question1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Question1"));
                    }
                }
            }
            get
            {
                return j5_Question1;
            }
        }
        public String J5_Question2
        {
            set
            {
                if (j5_Question2 != value)
                {
                    j5_Question2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Question2"));
                    }
                }
            }
            get
            {
                return j5_Question2;
            }
        }
        public String J5_ProgressBar
        {
            set
            {
                if (j5_ProgressBar != value)
                {
                    j5_ProgressBar = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_ProgressBar"));
                    }
                }
            }
            get
            {
                return j5_ProgressBar;
            }
        }
        public String TxtPuntos
        {
            set
            {
                if (j5_TxtPuntos != value)
                {
                    j5_TxtPuntos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_TxtPuntos"));
                    }
                }
            }
            get
            {
                return j5_TxtPuntos;
            }
        }
        public String ImagenPopup
        {
            set
            {
                if (imagenPopup != value)
                {
                    imagenPopup = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup"));
                    }
                }
            }
            get
            {
                return imagenPopup;
            }
        }

        public Color J5_ColorPregunta
        {
            set
            {
                if (j5_ColorPregunta != value)
                {
                    j5_ColorPregunta = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_ColorPregunta"));
                    }
                }
            }
            get
            {
                return j5_ColorPregunta;
            }
        }
        public Color J5_ColorCronometro
        {
            set
            {
                if (j5_ColorCronometro != value)
                {
                    j5_ColorCronometro = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_ColorCronometro"));
                    }
                }
            }
            get
            {
                return j5_ColorCronometro;
            }
        }
        
        public int J5_PosBtnAzul
        {
            set
            {
                if (j5_PosBtnAzul != value)
                {
                    j5_PosBtnAzul = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_PosBtnAzul"));
                    }
                }
            }
            get
            {
                return j5_PosBtnAzul;
            }
        }
        public int J5_PosBtnBlanco
        {
            set
            {
                if (j5_PosBtnBlanco != value)
                {
                    j5_PosBtnBlanco = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_PosBtnBlanco"));
                    }
                }
            }
            get
            {
                return j5_PosBtnBlanco;
            }
        }
        public int J5_PosBtnRojo
        {
            set
            {
                if (j5_PosBtnRojo != value)
                {
                    j5_PosBtnRojo = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_PosBtnRojo"));
                    }
                }
            }
            get
            {
                return j5_PosBtnRojo;
            }
        }

        public bool J5_VisibleStart
        {
            set
            {
                if (j5_VisibleStart != value)
                {
                    j5_VisibleStart = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_VisibleStart"));
                    }
                }
            }
            get
            {
                return j5_VisibleStart;
            }
        }
        public bool J5_Vitem1
        {
            set
            {
                if (j5_Vitem1 != value)
                {
                    j5_Vitem1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Vitem1"));
                    }
                }
            }
            get
            {
                return j5_Vitem1;
            }
        }
        public bool J5_Vitem2
        {
            set
            {
                if (j5_Vitem2 != value)
                {
                    j5_Vitem2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Vitem2"));
                    }
                }
            }
            get
            {
                return j5_Vitem2;
            }
        }
        public bool J5_Vitem3
        {
            set
            {
                if (j5_Vitem3 != value)
                {
                    j5_Vitem3 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Vitem3"));
                    }
                }
            }
            get
            {
                return j5_Vitem3;
            }
        }
        public bool J5_Vitem4
        {
            set
            {
                if (j5_Vitem4 != value)
                {
                    j5_Vitem4 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Vitem4"));
                    }
                }
            }
            get
            {
                return j5_Vitem4;
            }
        }
        public bool J5_Vitem5
        {
            set
            {
                if (j5_Vitem5 != value)
                {
                    j5_Vitem5 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J5_Vitem5"));
                    }
                }
            }
            get
            {
                return j5_Vitem5;
            }
        }

        //Funciones para Generar las Preguntas aleatorias (Devuelven 2 Strings)
        public String J5_generarPreguntas()
        {
            string[] preguntas = new string[2];

            preguntas[0] = "Pulsa el boton";
            preguntas[1] = "No pulses el boton";
         
            Array.Sort(preguntas);

            Random randNum = new Random();
            int val = randNum.Next(0, preguntas.Length);

            return preguntas[val];
        }
        public String J5_generarPreguntas2()
        {
            string[] preguntas2 = new string[3];

            preguntas2[0] = "AZUL";
            preguntas2[1] = "BLANCO";
            preguntas2[2] = "ROJO";

            Array.Sort(preguntas2);

            Random randNum = new Random();
            int val = randNum.Next(0, preguntas2.Length);

            return preguntas2[val];
        }

        //Funcion para generar la ProgressBar (Devuelve una Imagen segun la Puntuacion)
        private String J5_generarBarra()
        {
            String[] barra = new String[32];

            barra[0] = "J5_img/barra00.png";
            barra[1] = "J5_img/barra01.png";
            barra[2] = "J5_img/barra02.png";
            barra[3] = "J5_img/barra03.png";
            barra[4] = "J5_img/barra04.png";
            barra[5] = "J5_img/barra05.png";
            barra[6] = "J5_img/barra10.png";
            barra[7] = "J5_img/barra11.png";
            barra[8] = "J5_img/barra12.png";
            barra[9] = "J5_img/barra13.png";
            barra[10] = "J5_img/barra14.png";
            barra[11] = "J5_img/barra15.png";
            barra[12] = "J5_img/barra20.png";
            barra[13] = "J5_img/barra21.png";
            barra[14] = "J5_img/barra22.png";
            barra[15] = "J5_img/barra23.png";
            barra[16] = "J5_img/barra24.png";
            barra[17] = "J5_img/barra25.png";
            barra[18] = "J5_img/barra30.png";
            barra[19] = "J5_img/barra31.png";
            barra[20] = "J5_img/barra32.png";
            barra[21] = "J5_img/barra33.png";
            barra[22] = "J5_img/barra34.png";
            barra[23] = "J5_img/barra35.png";
            barra[24] = "J5_img/barra40.png";
            barra[25] = "J5_img/barra41.png";
            barra[26] = "J5_img/barra42.png";
            barra[27] = "J5_img/barra42.png";
            barra[28] = "J5_img/barra43.png";
            barra[29] = "J5_img/barra44.png";
            barra[30] = "J5_img/barra45.png";
            barra[31] = "J5_img/barra50.png";

            if (J5_Contador < 0)
            {
                J5_Contador = 0;
            }

            if (J5_Contador > 31)
            {
                J5_Contador = 31;
            }

            return barra[J5_Contador];


        }

        // Random para el COLOR de las Preguntas y la POSICION de los botones
        public void J5_randomColorPregunta()
        {
            Random randNum = new Random();
            int val = randNum.Next(0, 3);
            if(val == 0)
            {
                J5_ColorPregunta = Color.Red;
            }else if(val == 1)
            {
                J5_ColorPregunta = Color.White;
            }else if(val == 2)
            {
                J5_ColorPregunta = Color.Blue;
            }

        }

        public void J5_randomPosBtn()
        {
            Random r = new Random();
            int numRand = r.Next(0, 6);

            if (numRand == 0)
            {
                J5_PosBtnAzul = 0;
                J5_PosBtnBlanco = 0;
                J5_PosBtnRojo = 0;

            }
            else if (numRand == 1)
            {
                J5_PosBtnAzul = 185;
                J5_PosBtnBlanco = -185;
                J5_PosBtnRojo = 0;

            }
            else if (numRand == 2)
            {
                J5_PosBtnAzul = 185;
                J5_PosBtnBlanco = 185;
                J5_PosBtnRojo = -370;

            }
            else if (numRand == 3)
            {
                J5_PosBtnAzul = 0;
                J5_PosBtnBlanco = 185;
                J5_PosBtnRojo = -185;

            }
            else if (numRand == 4)
            {
                J5_PosBtnAzul = 370;
                J5_PosBtnBlanco = -185;
                J5_PosBtnRojo = -185;

            }
            else if (numRand == 5)
            {

                J5_PosBtnAzul = 370;
                J5_PosBtnBlanco = 0;
                J5_PosBtnRojo = -370;

            }

        }

        Boolean blanco = false;
        Boolean azul = false;
        Boolean rojo = false;

        // Funcion de INICIO y FINAL del JUEGO
        private void J5_StartGame()
        {
            blanco = true;
            rojo = true;
            azul = true;
            J5_VisibleStart = false;
            J5_Cronometrar();
            J5_nuevaPregunta();
        }
        public async void J5_EndGame()
        {
            if (MainPageViewModel.playedGames == 5)
            {
                PopupNavigation.Instance.PopAsync();
                MainPageViewModel h = new MainPageViewModel("", Navigation);
                h.PopUp();


            }
            else
            {
                PopupNavigation.Instance.PopAsync();
                await Navigation.PushModalAsync(new MainPage());
            }
        }

        public void soundCorrect()
        {
            var assembly1 = typeof(App).GetTypeInfo().Assembly;
            Stream audioStream1 = assembly1.GetManifestResourceStream("WorldMiniGamesV1.Sounds.correct.mp3");
            var audio1 = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
            audio1.Load(audioStream1);
            audio1.Play();
        }

        public void soundWrong()
        {
            var assembly2 = typeof(App).GetTypeInfo().Assembly;
            Stream audioStream2 = assembly2.GetManifestResourceStream("WorldMiniGamesV1.Sounds.wrong.mp3");
            var audio2 = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
            audio2.Load(audioStream2);
            audio2.Play();
        }

        //Funciones al hacer CLICK en cada uno de los botones
        public void J5_clickAzul()
        {
            if (azul)
            {
                if (J5_Question2 == "AZUL" && J5_Question1 == "Pulsa el boton")
                {
                    J5_Contador++;
                    soundCorrect();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta(); 
                }
                else if ((J5_Question2 == "BLANCO" || J5_Question2 == "ROJO") && J5_Question1 == "No pulses el boton")
                {
                    J5_Contador++;
                    soundCorrect();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta(); 
                }
                else
                {
                    J5_Contador -= 5;
                    soundWrong();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta();
                }
            }
        }
        public void J5_clickBlanco()
        {
            if (blanco) {            
                if (J5_Question2 == "BLANCO" && J5_Question1 == "Pulsa el boton")
                {
                    J5_Contador++;
                    soundCorrect();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta();
                }
            
                else if ((J5_Question2 == "AZUL" || J5_Question2 == "ROJO") && J5_Question1 == "No pulses el boton")
                {
                    J5_Contador++;
                    soundCorrect();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta(); 
                }
                else
                {
                    J5_Contador -= 5;
                    soundWrong();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta();
                }
            }
        }
        public void J5_clickRojo()
        {
            if (rojo)
            {
                if (J5_Question2 == "ROJO" && J5_Question1 == "Pulsa el boton")
                {
                    J5_Contador++;
                    soundCorrect();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta(); 
                }

                else if ((J5_Question2 == "BLANCO" || J5_Question2 == "AZUL") && J5_Question1 == "No pulses el boton")
                {
                    J5_Contador++;
                    soundCorrect();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta();
                }
                else
                {
                    J5_Contador -= 5;
                    soundWrong();
                    J5_ProgressBar = J5_generarBarra();
                    J5_nuevaPregunta();
                }
            }
        }

        // Genera una nueva pregunta y le asigna un COLOR y una POSICION
        public void J5_nuevaPregunta()
        {
            J5_Question1 = J5_generarPreguntas();
            J5_Question2 = J5_generarPreguntas2();
            J5_randomColorPregunta();
            J5_randomPosBtn();

        }
        public void J5_mostrarItems()
        {
            if (J5_Contador == 6)
            {
                J5_Vitem1 = true;
            }
            else if (J5_Contador == 12)
            {
                J5_Vitem2 = true;
            }
            else if (J5_Contador == 18)
            {
                J5_Vitem3 = true;
            }
            else if (J5_Contador == 24)
            {
                J5_Vitem4 = true;
            }
            else if (J5_Contador == 30)
            {
                J5_Vitem5 = true;
            }
        }

        // Funcion de Cronometrar 30 segundos, Cambiar el COLOR del Cronometro, Mostrar los ITEMS conseguidos y Desplegar POPUP al final de la partida
        private async void J5_Cronometrar()
        {
            Color[] ABR = new Color[3];
            int x = 0;

            ABR[0] = Color.Blue;
            ABR[1] = Color.White;
            ABR[2] = Color.Red;

            for (int i = 0; i < 30; i++)
            {
                J5_ColorCronometro = ABR[x];
                J5_Cronometro--;
                await Task.Delay(1000);
                x++;
                if (x > 2)
                {
                    x = 0;
                }

                if (J5_Contador >= 31 || J5_Cronometro <= 0)
                {
                    PopUp();
                    
                }
                J5_mostrarItems();
            }

         
        }

        //Funcion de creacion del POPUP, su ANIMACION y sus PROPIEDADES
        private async void PopUp()
        {
            MainPageViewModel.puntuacionTotal += J5_Contador;
            MainPageViewModel.playedGames++;
            TxtPuntos = "PUNTUACIÓN: " + J5_Contador + " PUNTOS";
            var pr = new PopUp(TxtPuntos, Navigation);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left
               
            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);

        }

        //Funcion Bonus al consegui X puntos
        //public void J5_BonusPuntos()
        //{
        //    if (J5_Contador == 10)
        //    {
        //        J5_Cronometro = J5_Cronometro + 5;
        //    }else if (J5_Contador == 20)
        //    {
        //        J5_Cronometro = J5_Cronometro + 10;
        //    }else if (J5_Contador == 30)
        //    {
        //        J5_Contador = J5_Contador + 10;
        //    }
        //}
    }
}