﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    public class TicToeViewModel : ContentPage, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public INavigation Navigation { get; set; }

        //Comandos biding
        public ICommand CambiarImagen1 { get; set; }
        public ICommand CambiarImagen2 { get; set; }
        public ICommand CambiarImagen3 { get; set; }
        public ICommand CambiarImagen4 { get; set; }
        public ICommand CambiarImagen5 { get; set; }
        public ICommand CambiarImagen6 { get; set; }
        public ICommand CambiarImagen7 { get; set; }
        public ICommand CambiarImagen8 { get; set; }
        public ICommand CambiarImagen9 { get; set; }
        public ICommand CerrarApp { get; set; }

        //Variables String de las imagenes
        public String imagenClick1;
        public String imagenClick2;
        public String imagenClick3;
        public String imagenClick4;
        public String imagenClick5;
        public String imagenClick6;
        public String imagenClick7;
        public String imagenClick8;
        public String imagenClick9;

        //Variables bool para habilitar y deshabilitar imagenes
        public bool enableImg1;
        public bool enableImg2;
        public bool enableImg3;
        public bool enableImg4;
        public bool enableImg5;
        public bool enableImg6;
        public bool enableImg7;
        public bool enableImg8;
        public bool enableImg9;

        //Variables int para puntuacion, cronometro y clicks
        public int puntuacion1;
        public int puntuacion2;
        public int cronometro;
        public int clicks;
        public int puntuacionTotal;

        //Variables bool para comprobar la IA y el resultado de la partida
        public bool comprobarRand1;
        public bool vGanado;
        public bool vPerdido;
        public bool vEmpate;
        public bool win1;
        public bool win2;

        //Imagen del jugador1 y del jugador2
        public string imagenSeñor = "J10_img/señor1.png";
        public string imagenSeñora = "J10_img/señora1.png";

        public String txtPuntos;
        public String imagenPopup = "J10_img/chico.png";

        Random r = new Random();

        //Constructor para inicializar variables y Comandos
        public TicToeViewModel(String txtPuntos, INavigation navigation)
        {
            this.Navigation = navigation;
            this.TxtPuntos = txtPuntos;
            ImagenClick1 = "J10_img/tapa.png";
            ImagenClick2 = "J10_img/tapa.png";
            ImagenClick3 = "J10_img/tapa.png";
            ImagenClick4 = "J10_img/tapa.png";
            ImagenClick5 = "J10_img/tapa.png";
            ImagenClick6 = "J10_img/tapa.png";
            ImagenClick7 = "J10_img/tapa.png";
            ImagenClick8 = "J10_img/tapa.png";
            ImagenClick9 = "J10_img/tapa.png";

            EnableImg1 = true;
            EnableImg2 = true;
            EnableImg3 = true;
            EnableImg4 = true;
            EnableImg5 = true;
            EnableImg6 = true;
            EnableImg7 = true;
            EnableImg8 = true;
            EnableImg9 = true;

            Puntuacion1 = 0;
            Puntuacion2 = 0;
            Cronometro = 30;
            clicks = 0;

            win1 = false;
            win2 = false;

            CambiarImagen1 = new Command(cambiarImagen1);
            CambiarImagen2 = new Command(cambiarImagen2);
            CambiarImagen3 = new Command(cambiarImagen3);
            CambiarImagen4 = new Command(cambiarImagen4);
            CambiarImagen5 = new Command(cambiarImagen5);
            CambiarImagen6 = new Command(cambiarImagen6);
            CambiarImagen7 = new Command(cambiarImagen7);
            CambiarImagen8 = new Command(cambiarImagen8);
            CambiarImagen9 = new Command(cambiarImagen9);
            CerrarApp = new Command(J10_EndGame);

            comprobarRand1 = false;
            VGanado = false;
            VPerdido = false;
            VEmpate = false;

            Cronometrar();


        }

        //Bindings
        public String ImagenClick1
        {
            set
            {
                if (imagenClick1 != value)
                {
                    imagenClick1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick1"));
                    }
                }
            }
            get
            {
                return imagenClick1;
            }
        }
        public String ImagenClick2
        {
            set
            {
                if (imagenClick2 != value)
                {
                    imagenClick2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick2"));
                    }
                }
            }
            get
            {
                return imagenClick2;
            }
        }
        public String ImagenClick3
        {
            set
            {
                if (imagenClick3 != value)
                {
                    imagenClick3 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick3"));
                    }
                }
            }
            get
            {
                return imagenClick3;
            }
        }
        public String ImagenClick4
        {
            set
            {
                if (imagenClick4 != value)
                {
                    imagenClick4 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick4"));
                    }
                }
            }
            get
            {
                return imagenClick4;
            }
        }
        public String ImagenClick5
        {
            set
            {
                if (imagenClick5 != value)
                {
                    imagenClick5 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick5"));
                    }
                }
            }
            get
            {
                return imagenClick5;
            }
        }
        public String ImagenClick6
        {
            set
            {
                if (imagenClick6 != value)
                {
                    imagenClick6 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick6"));
                    }
                }
            }
            get
            {
                return imagenClick6;
            }
        }
        public String ImagenClick7
        {
            set
            {
                if (imagenClick7 != value)
                {
                    imagenClick7 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick7"));
                    }
                }
            }
            get
            {
                return imagenClick7;
            }
        }
        public String ImagenClick8
        {
            set
            {
                if (imagenClick8 != value)
                {
                    imagenClick8 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick8"));
                    }
                }
            }
            get
            {
                return imagenClick8;
            }
        }
        public String ImagenClick9
        {
            set
            {
                if (imagenClick9 != value)
                {
                    imagenClick9 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenClick9"));
                    }
                }
            }
            get
            {
                return imagenClick9;
            }
        }

        public String TxtPuntos
        {
            set
            {
                if (txtPuntos != value)
                {
                    txtPuntos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TxtPuntos"));
                    }
                }
            }
            get
            {
                return txtPuntos;
            }
        }
        public String ImagenPopup
        {
            set
            {
                if (imagenPopup != value)
                {
                    imagenPopup = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup"));
                    }
                }
            }
            get
            {
                return imagenPopup;
            }
        }

        public bool EnableImg1
        {
            set
            {
                if (enableImg1 != value)
                {
                    enableImg1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg1"));
                    }
                }
            }
            get
            {
                return enableImg1;
            }
        }
        public bool EnableImg2
        {
            set
            {
                if (enableImg2 != value)
                {
                    enableImg2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg2"));
                    }
                }
            }
            get
            {
                return enableImg2;
            }
        }
        public bool EnableImg3
        {
            set
            {
                if (enableImg3 != value)
                {
                    enableImg3 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg3"));
                    }
                }
            }
            get
            {
                return enableImg3;
            }
        }
        public bool EnableImg4
        {
            set
            {
                if (enableImg4 != value)
                {
                    enableImg4 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg4"));
                    }
                }
            }
            get
            {
                return enableImg4;
            }
        }
        public bool EnableImg5
        {
            set
            {
                if (enableImg5 != value)
                {
                    enableImg5 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg5"));
                    }
                }
            }
            get
            {
                return enableImg5;
            }
        }
        public bool EnableImg6
        {
            set
            {
                if (enableImg6 != value)
                {
                    enableImg6 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg6"));
                    }
                }
            }
            get
            {
                return enableImg6;
            }
        }
        public bool EnableImg7
        {
            set
            {
                if (enableImg7 != value)
                {
                    enableImg7 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg7"));
                    }
                }
            }
            get
            {
                return enableImg7;
            }
        }
        public bool EnableImg8
        {
            set
            {
                if (enableImg8 != value)
                {
                    enableImg8 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg8"));
                    }
                }
            }
            get
            {
                return enableImg8;
            }
        }
        public bool EnableImg9
        {
            set
            {
                if (enableImg9 != value)
                {
                    enableImg9 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("EnableImg9"));
                    }
                }
            }
            get
            {
                return enableImg9;
            }
        }

        public bool VGanado
        {
            set
            {
                if (vGanado != value)
                {
                    vGanado = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("VGanado"));
                    }
                }
            }
            get
            {
                return vGanado;
            }
        }
        public bool VPerdido
        {
            set
            {
                if (vPerdido != value)
                {
                    vPerdido = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("VPerdido"));
                    }
                }
            }
            get
            {
                return vPerdido;
            }
        }
        public bool VEmpate
        {
            set
            {
                if (vEmpate != value)
                {
                    vEmpate = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("VEmpate"));
                    }
                }
            }
            get
            {
                return vEmpate;
            }
        }

        public int Puntuacion1
        {
            set
            {
                if (puntuacion1 != value)
                {
                    puntuacion1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Puntuacion1"));
                    }
                }
            }
            get
            {
                return puntuacion1;
            }
        }
        public int Puntuacion2
        {
            set
            {
                if (puntuacion2 != value)
                {
                    puntuacion2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Puntuacion2"));
                    }
                }
            }
            get
            {
                return puntuacion2;
            }
        }
        public int Cronometro
        {
            set
            {
                if (cronometro != value)
                {
                    cronometro = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Cronometro"));
                    }
                }
            }
            get
            {
                return cronometro;
            }
        }


        //Funciones al dar click a las imagenes
        public void cambiarImagen1()
        {
            ImagenClick1 = imagenSeñor;
            EnableImg1 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();

        }
        public void cambiarImagen2()
        {
            ImagenClick2 = imagenSeñor;
            EnableImg2 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();


        }
        public void cambiarImagen3()
        {
            ImagenClick3 = imagenSeñor;
            EnableImg3 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();


        }
        public void cambiarImagen4()
        {
            ImagenClick4 = imagenSeñor;
            EnableImg4 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();

        }
        public void cambiarImagen5()
        {
            ImagenClick5 = imagenSeñor;
            EnableImg5 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();

        }
        public void cambiarImagen6()
        {
            ImagenClick6 = imagenSeñor;
            EnableImg6 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();

        }
        public void cambiarImagen7()
        {
            ImagenClick7 = imagenSeñor;
            EnableImg7 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();

        }
        public void cambiarImagen8()
        {
            ImagenClick8 = imagenSeñor;
            EnableImg8 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();

        }
        public void cambiarImagen9()
        {
            ImagenClick9 = imagenSeñor;
            EnableImg9 = false;
            //await Task.Delay(50);
            comprobarRand1 = false;
            clickIA();

        }

        //Funcion clicks de la IA
        public void clickIA()
        {
            
            int numRand;
            clicks++;

            if (clicks == 5)
            {
                comprobarRand1 = true;
            }

            while (comprobarRand1 == false)
            {
                numRand = r.Next(1, 10);

                if (numRand == 1)
                {
                    if (ImagenClick1 == imagenSeñor || ImagenClick1 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick1 = imagenSeñora;
                        EnableImg1 = false;
                        comprobarRand1 = true;
                    }
                }
                if (numRand == 2)
                {
                    if (ImagenClick2 == imagenSeñor || ImagenClick2 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick2 = imagenSeñora;
                        EnableImg2 = false;
                        comprobarRand1 = true;
                    }

                }
                if (numRand == 3)
                {
                    if (ImagenClick3 == imagenSeñor || ImagenClick3 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick3 = imagenSeñora;
                        EnableImg3 = false;
                        comprobarRand1 = true;
                    }

                }

                if (numRand == 4)
                {
                    if (ImagenClick4 == imagenSeñor || ImagenClick4 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick4 = imagenSeñora;
                        EnableImg4 = false;
                        comprobarRand1 = true;
                    }

                }
                if (numRand == 5)
                {
                    if (ImagenClick5 == imagenSeñor || ImagenClick5 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick5 = imagenSeñora;
                        EnableImg5 = false;
                        comprobarRand1 = true;
                    }

                }

                if (numRand == 6)
                {
                    if (ImagenClick6 == imagenSeñor || ImagenClick6 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick6 = imagenSeñora;
                        EnableImg6 = false;
                        comprobarRand1 = true;
                    }

                }

                if (numRand == 7)
                {
                    if (ImagenClick7 == imagenSeñor || ImagenClick7 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick7 = imagenSeñora;
                        EnableImg7 = false;
                        comprobarRand1 = true;
                    }

                }

                if (numRand == 8)
                {
                    if (ImagenClick8 == imagenSeñor || ImagenClick8 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick8 = imagenSeñora;
                        EnableImg8 = false;
                        comprobarRand1 = true;
                    }

                }
                if (numRand == 9)
                {
                    if (ImagenClick9 == imagenSeñor || ImagenClick9 == imagenSeñora)
                    {
                        comprobarRand1 = false;
                    }
                    else
                    {
                        ImagenClick9 = imagenSeñora;
                        EnableImg9 = false;
                        comprobarRand1 = true;
                    }

                }
            }

            comprobarGanador();
        }

        //Funcion para comprobar ganador
        public async void comprobarGanador()
        {
            
            if (ImagenClick1 == imagenSeñor && ImagenClick2 == imagenSeñor && ImagenClick3 == imagenSeñor) { win1 = true; }
            if (ImagenClick3 == imagenSeñor && ImagenClick6 == imagenSeñor && ImagenClick9 == imagenSeñor) { win1 = true; }
            if (ImagenClick7 == imagenSeñor && ImagenClick8 == imagenSeñor && ImagenClick9 == imagenSeñor) { win1 = true; }
            if (ImagenClick1 == imagenSeñor && ImagenClick4 == imagenSeñor && ImagenClick7 == imagenSeñor) { win1 = true; }
            if (ImagenClick1 == imagenSeñor && ImagenClick5 == imagenSeñor && ImagenClick9 == imagenSeñor) { win1 = true; }
            if (ImagenClick3 == imagenSeñor && ImagenClick5 == imagenSeñor && ImagenClick7 == imagenSeñor) { win1 = true; }
            if (ImagenClick4 == imagenSeñor && ImagenClick5 == imagenSeñor && ImagenClick6 == imagenSeñor) { win1 = true; }
            if (ImagenClick2 == imagenSeñor && ImagenClick5 == imagenSeñor && ImagenClick8 == imagenSeñor) { win1 = true; }

            if (ImagenClick1 == imagenSeñora && ImagenClick2 == imagenSeñora && ImagenClick3 == imagenSeñora) { win2 = true; }
            if (ImagenClick3 == imagenSeñora && ImagenClick6 == imagenSeñora && ImagenClick9 == imagenSeñora) { win2 = true; }
            if (ImagenClick7 == imagenSeñora && ImagenClick8 == imagenSeñora && ImagenClick9 == imagenSeñora) { win2 = true; }
            if (ImagenClick1 == imagenSeñora && ImagenClick4 == imagenSeñora && ImagenClick7 == imagenSeñora) { win2 = true; }
            if (ImagenClick1 == imagenSeñora && ImagenClick5 == imagenSeñora && ImagenClick9 == imagenSeñora) { win2 = true; }
            if (ImagenClick3 == imagenSeñora && ImagenClick5 == imagenSeñora && ImagenClick7 == imagenSeñora) { win2 = true; }
            if (ImagenClick4 == imagenSeñora && ImagenClick5 == imagenSeñora && ImagenClick6 == imagenSeñora) { win2 = true; }
            if (ImagenClick2 == imagenSeñora && ImagenClick5 == imagenSeñora && ImagenClick8 == imagenSeñora) { win2 = true; }

            if (win1 == true)
            {
                VGanado = true;
                Puntuacion1++;
                await Task.Delay(1000);
                resetear();
            }
            else if (win2 == true)
            {
                VPerdido = true;
                Puntuacion2++;
                await Task.Delay(1000);
                resetear();
            }
            else if (clicks == 5 && win1 == false && win2 == false)
            {
                VEmpate = true;
                await Task.Delay(1000);
                resetear();
            }
        }

        //Funcion para resetear partida
        public void resetear()
        {

            ImagenClick1 = "J10_img/tapa.png";
            ImagenClick2 = "J10_img/tapa.png";
            ImagenClick3 = "J10_img/tapa.png";
            ImagenClick4 = "J10_img/tapa.png";
            ImagenClick5 = "J10_img/tapa.png";
            ImagenClick6 = "J10_img/tapa.png";
            ImagenClick7 = "J10_img/tapa.png";
            ImagenClick8 = "J10_img/tapa.png";
            ImagenClick9 = "J10_img/tapa.png";

            EnableImg1 = true;
            EnableImg2 = true;
            EnableImg3 = true;
            EnableImg4 = true;
            EnableImg5 = true;
            EnableImg6 = true;
            EnableImg7 = true;
            EnableImg8 = true;
            EnableImg9 = true;

            comprobarRand1 = false;
            VGanado = false;
            VPerdido = false;
            VEmpate = false;

            clicks = 0;
            win1 = false;
            win2 = false;
        }

        //Funcion Async Cronometro
        public async Task Cronometrar()
        {

            for (int i = 0; i < 30; i++)
            {
                Cronometro--;
                await Task.Delay(1000);
            }

            if (MainPageViewModel.bugTicToc == 0)
            {
                MainPageViewModel.bugTicToc = 1;
                PopUp();
            }
        }

        private async void PopUp()
        {
            puntuacionTotal = Puntuacion1 - Puntuacion2;
            MainPageViewModel.puntuacionTotal += puntuacionTotal;
            MainPageViewModel.playedGames++;
            puntuacionTotal = puntuacionTotal * 2;
            TxtPuntos = "PUNTUACIÓN: " + puntuacionTotal + " PUNTOS";
            var pr = new PopUp(TxtPuntos, Navigation, 0.0f);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left

            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);

        }

        public async void J10_EndGame()
        {
            if (MainPageViewModel.playedGames == 5)
            {
                PopupNavigation.Instance.PopAsync();
                MainPageViewModel h = new MainPageViewModel("", Navigation);
                h.PopUp();
            }
            else
            {
                PopupNavigation.Instance.PopAsync();
                await Navigation.PushModalAsync(new MainPage());
            }
        }

    }
}