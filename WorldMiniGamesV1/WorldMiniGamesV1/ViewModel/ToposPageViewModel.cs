﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WorldMiniGamesV1.View;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{

    public class ToposPageViewModel : INotifyPropertyChanged
    {
        
        public event PropertyChangedEventHandler PropertyChanged;
        public INavigation Navigation { get; set; }
        public ICommand ComandoContador1 { get; set; }      /*COMANDO DEL CONTADOR*/
        public ICommand ComandoIniciarTopos { get; set; }      /*COMANDO DE INICIAR PARTIDA*/

        public ICommand CerrarApp { get; set; }

        /*TODAS LAS VARIABLES INICIALIZADAS*/
        public int contador1; 
        public int incremento; 
        public int restador; 
        public int cronometro;

        public String imagen1; 
        public String imagen2; 
        public String imagen3; 
        public String imagen4; 
        public String imagen5; 
        public String imagen6; 
        public String imagen7; 
        public String imagen8; 
        public String imagen9;
        public String imagenPopup = "J4_img/canguro.png";
        public String txtPuntos;

        public bool v_BotonStart = true; 
        public bool e_BotonStart = true;

        public bool t1; 
        public bool t2; 
        public bool t3; 
        public bool t4; 
        public bool t5; 
        public bool t6; 
        public bool t7; 
        public bool t8; 
        public bool t9;

        public bool infoToposB =true; 
        public bool infoToposM = true; 
        public bool vReloj;

        public double scaleLbl;

        public bool b1 = true;
        public bool b2 = true;
        public bool b3 = true;

        public bool vbonus = false;

        

        public ToposPageViewModel()
        {
            ComandoContador1 = new Command(clickcont1); 
            ComandoIniciarTopos = new Command(IniciarPartida); /*INCIALIZAR TODOS LOS COMANDOS ANTERIORES*/
            CerrarApp = new Command(J4_EndGame);
        }

        public ToposPageViewModel(String txtPuntos, INavigation navigation)
        {
            this.TxtPuntos = txtPuntos;
            this.Navigation = navigation;

            ComandoContador1 = new Command(clickcont1);
            ComandoIniciarTopos = new Command(IniciarPartida); /*INCIALIZAR TODOS LOS COMANDOS ANTERIORES*/
            CerrarApp = new Command(J4_EndGame);
        }

        /*GET Y SET DE LAS IMAGENES DEL JUEGO*/
        public String Imagen1
        {
            set
            {
                if (imagen1 != value)
                {
                    imagen1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen1"));
                    }
                }
            }
            get
            {
                return imagen1;
            }
        }
        
        public String Imagen2
        {
            set
            {
                if (imagen2 != value)
                {
                    imagen2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen2"));
                    }
                }
            }
            get
            {
                return imagen2;
            }
        }
        public String Imagen3
        {
            set
            {
                if (imagen3 != value)
                {
                    imagen3 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen3"));
                    }
                }
            }
            get
            {
                return imagen3;
            }
        }
        public String Imagen4
        {
            set
            {
                if (imagen4 != value)
                {
                    imagen4 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen4"));
                    }
                }
            }
            get
            {
                return imagen4;
            }
        }
        public String Imagen5
        {
            set
            {
                if (imagen5 != value)
                {
                    imagen5 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen5"));
                    }
                }
            }
            get
            {
                return imagen5;
            }
        }
        public String Imagen6
        {
            set
            {
                if (imagen6 != value)
                {
                    imagen6 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen6"));
                    }
                }
            }
            get
            {
                return imagen6;
            }
        }
        public String Imagen7
        {
            set
            {
                if (imagen7 != value)
                {
                    imagen7 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen7"));
                    }
                }
            }
            get
            {
                return imagen7;
            }
        }
        public String Imagen8
        {
            set
            {
                if (imagen8 != value)
                {
                    imagen8 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen8"));
                    }
                }
            }
            get
            {
                return imagen8;
            }
        }
        public String Imagen9
        {
            set
            {
                if (imagen9 != value)
                {
                    imagen9 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Imagen9"));
                    }
                }
            }
            get
            {
                return imagen9;
            }
        }
        public String ImagenPopup
        {
            set
            {
                if (imagenPopup != value)
                {
                    imagenPopup = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup"));
                    }
                }
            }
            get
            {
                return imagenPopup;
            }
        }
        public String TxtPuntos
        {
            set
            {
                if (txtPuntos != value)
                {
                    txtPuntos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TxtPuntos"));
                    }
                }
            }
            get
            {
                return txtPuntos;
            }
        }

        /*GET Y SET DEL ISVISIBLE DE LAS IMAGENES*/
        public bool T1
        {
            set
            {
                if (t1 != value)
                {
                    t1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T1"));
                    }
                }
            }
            get
            {
                return t1;
            }
        }
        public bool T2
        {
            set
            {
                if (t2 != value)
                {
                    t2 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T2"));
                    }
                }
            }
            get
            {
                return t2;
            }
        }
        public bool T3
        {
            set
            {
                if (t3 != value)
                {
                    t3 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T3"));
                    }
                }
            }
            get
            {
                return t3;
            }
        }
        public bool T4
        {
            set
            {
                if (t4 != value)
                {
                    t4 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T4"));
                    }
                }
            }
            get
            {
                return t4;
            }
        }
        public bool T5
        {
            set
            {
                if (t5 != value)
                {
                    t5 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T5"));
                    }
                }
            }
            get
            {
                return t5;
            }
        }
        public bool T6
        {
            set
            {
                if (t6 != value)
                {
                    t6 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T6"));
                    }
                }
            }
            get
            {
                return t6;
            }
        }
        public bool T7
        {
            set
            {
                if (t7 != value)
                {
                    t7 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T7"));
                    }
                }
            }
            get
            {
                return t7;
            }
        }
        public bool T8
        {
            set
            {
                if (t8 != value)
                {
                    t8 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T8"));
                    }
                }
            }
            get
            {
                return t8;
            }
        }
        public bool T9
        {
            set
            {
                if (t9 != value)
                {
                    t9 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("T9"));
                    }
                }
            }
            get
            {
                return t9;
            }
        }

        /*GET Y SET PARA MOSTRAR O NO LA INFO INICIAL*/
        public bool InfoToposB
        {
            set
            {
                if (infoToposB != value)
                {
                    infoToposB = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("InfoToposB"));
                    }
                }
            }
            get
            {
                return infoToposB;
            }
        }
        public bool InfoToposM
        {
            set
            {
                if (infoToposM != value)
                {
                    infoToposM = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("InfoToposM"));
                    }
                }
            }
            get
            {
                return infoToposM;
            }
        }

        public bool VReloj
        {
            set
            {
                if (vReloj != value)
                {
                    vReloj = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("VReloj"));
                    }
                }
            }
            get
            {
                return vReloj;
            }
        }

        /*GET Y SET DEL ISVISIBLE Y EL ISENABLE DEL BOTON DE START*/
        public bool V_BotonStart
        {
            set
            {
                if (v_BotonStart != value)
                {
                    v_BotonStart = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("V_BotonStart"));
                    }
                }
            }
            get
            {
                return v_BotonStart;
            }
        }
        public bool E_BotonStart
        {
            set
            {
                if (e_BotonStart != value)
                {
                    e_BotonStart = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("E_BotonStart"));
                    }
                }
            }
            get
            {
                return e_BotonStart;
            }
        }

        /*GET Y SET DEL CONTADOR Y EL CRONOMETRO*/
        public int Contador1
        {
            set
            {
                if (contador1 != value)
                {
                    contador1 = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Contador1"));
                    }
                }
            }
            get
            {
                return contador1;
            }
        }
        public int Cronometro
        {
            set
            {
                if (cronometro != value)
                {
                    cronometro = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Cronometro"));
                    }
                }
            }
            get
            {
                return cronometro;
            }
        }

        public double ScaleLbl
        {
            set
            {
                if (scaleLbl != value)
                {
                    scaleLbl = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ScaleLbl"));
                    }
                }
            }
            get
            {
                return scaleLbl;
            }
        }
        public bool Vbonus
        {
            set
            {
                if (vbonus != value)
                {
                    vbonus = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("Vbonus"));
                    }
                }
            }
            get
            {
                return vbonus;
            }
        }

        /*FUNCION DE INICIAR PARTIDA*/
        public void IniciarPartida()
        {
            incremento = 0; Cronometro = 30;
            V_BotonStart = false; E_BotonStart = false;InfoToposB = false; InfoToposM = false; VReloj = true;
            Imagen1 = "J4_img/agujero_sintopo.png"; 
            Imagen2 = "J4_img/agujero_sintopo.png"; 
            Imagen3 = "J4_img/agujero_sintopo.png"; 
            Imagen4 = "J4_img/agujero_sintopo.png"; 
            Imagen5 = "J4_img/agujero_sintopo.png"; 
            Imagen6 = "J4_img/agujero_sintopo.png"; 
            Imagen7 = "J4_img/agujero_sintopo.png";
            Imagen8 = "J4_img/agujero_sintopo.png";
            Imagen9 = "J4_img/agujero_sintopo.png";            
            T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
            
            IniciarCronometro();
            mostrarTopos();
        }

        /*FUNCION DEl CONTADOR*/
        public async void clickcont1()
        {
            
            if (incremento == 1)
            {
                Contador1 = Contador1 + incremento;
                T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                var assembly = typeof(App).GetTypeInfo().Assembly;
                Stream audioStream = assembly.GetManifestResourceStream("WorldMiniGamesV1.Sounds.correct.mp3");
                var audio = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
                audio.Load(audioStream);
                audio.Play();
            }
            else
            {
                Contador1 = Contador1 + incremento;
                T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                var assembly = typeof(App).GetTypeInfo().Assembly;
                Stream audioStream = assembly.GetManifestResourceStream("WorldMiniGamesV1.Sounds.wrong.mp3");
                var audio = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
                audio.Load(audioStream);
                audio.Play();
            }
            await AnimarPuntuacion();
            Bonus();
            

        }

        public async Task AnimarPuntuacion()
        {
            ScaleLbl = 1.5;
            await Task.Delay(10);
            ScaleLbl = 1;
        }

        public async Task Bonus() {

            if (Contador1 >= 10 && b1 == true)
            {
                Cronometro += 5;
                b1 = false;
                Vbonus = true;
                await Task.Delay(1000);
                Vbonus = false;
            }
            if (Contador1 >= 20 && b2 == true) 
            {
                Cronometro += 5;
                b2 = false;
                Vbonus = true;
                await Task.Delay(1000);
                Vbonus = false;
            }
            if (Contador1 >= 30 && b3 == true)
            {
                Cronometro += 5;
                b3 = false;
                Vbonus = true;
                await Task.Delay(1000);
                Vbonus = false;
            }
        }

        /*FUNCION DE INICIAR CRONOMETRO*/
        public async Task IniciarCronometro()
        {            
            do 
            {
                Cronometro--;
                await Task.Delay(1000);
                
            } while (Cronometro !=0) ;

            if (Cronometro == 0)
            {
                Cronometro = 0;
                PopUp();
            }


        }

        //public async void Stop()
        //{
            
        //    await Task.Delay(1000);
        //    _ = IniciarCronometro();

        //}

        /*FUNCION PARA QUE SE MUESTREN LOS TOPOS DE FORMA RANDOM*/

      
        public async Task mostrarTopos()
        {

            Random r = new Random();
            int delay = 1000;
            int restaDelay = 10;
         
            while (true)
            {
                int numt = r.Next(0, 13);
                //Topos Buenos
                if (numt == 0)
                {
                    Imagen1 = "J4_img/topo1.png";
                    T1 = true; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                else if (numt == 1)
                {
                    Imagen2 = "J4_img/topo2.png";
                    T1 = false; T2 = true; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                else if (numt == 2)
                {
                    Imagen3 = "J4_img/topo3.png";
                    T1 = false; T2 = false; T3 = true; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                else if (numt == 3)
                {
                    Imagen4 = "J4_img/topo4.png";
                    T1 = false; T2 = false; T3 = false; T4 = true; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                else if (numt == 4)
                {
                    Imagen5 = "J4_img/topo5.png";
                    T1 = false; T2 = false; T3 = false; T4 = false; T5 = true; T6 = false; T7 = false; T8 = false; T9 = false;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                else if (numt == 5)
                {
                    Imagen6 = "J4_img/topo6.png";
                    T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = true; T7 = false; T8 = false; T9 = false;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                else if (numt == 6)
                {
                    Imagen7 = "J4_img/topo7.png";
                    T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = true; T8 = false; T9 = false;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                else if (numt == 7)
                {
                    Imagen8 = "J4_img/topo8.png";
                    T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = true; T9 = false;
                    incremento = 1;

                    delay = delay - restaDelay;
                }
                else if (numt == 8)
                {
                    Imagen9 = "J4_img/topo9.png";
                    T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = true;
                    incremento = 1;
                    delay = delay - restaDelay;
                }
                //Topos malos
                else if (numt == 9 || numt == 10 || numt == 11 || numt == 12 || numt == 13)
                {
                    int numt2 = r.Next(0, 9);
                    if(numt2 == 0)
                    {
                        Imagen1 = "J4_img/topo_malo.png";
                        T1 = true; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 1)
                    {
                        Imagen2 = "J4_img/topo_malo.png";
                        T1 = false; T2 = true; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 2)
                    {
                        Imagen3 = "J4_img/topo_malo.png";
                        T1 = false; T2 = false; T3 = true; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 3)
                    {
                        Imagen4 = "J4_img/topo_malo.png";
                        T1 = false; T2 = false; T3 = false; T4 = true; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 4)
                    {
                        Imagen6 = "J4_img/topo_malo.png";
                        T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = true; T7 = false; T8 = false; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 5)
                    {
                        Imagen7 = "J4_img/topo_malo.png";
                        T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = true; T8 = false; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 6)
                    {
                        Imagen8 = "J4_img/topo_malo.png";
                        T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = true; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 7)
                    {
                        Imagen9 = "J4_img/topo_malo.png";
                        T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = true;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }
                    else if (numt == 8)
                    {
                        Imagen5 = "J4_img/topo_malo.png";
                        T1 = false; T2 = false; T3 = false; T4 = false; T5 = true; T6 = false; T7 = false; T8 = false; T9 = false;
                        incremento = -5;
                        delay = delay + restaDelay;
                    }

                }
                
                if(delay == 200)
                {
                    delay = 200;
                }
                /*REINICIO LAS VARIABLES AQUI*/
                await Task.Delay(delay);
                Imagen1 = "J4_img/agujero_sintopo.png";
                Imagen2 = "J4_img/agujero_sintopo.png";
                Imagen3 = "J4_img/agujero_sintopo.png";
                Imagen4 = "J4_img/agujero_sintopo.png";
                Imagen5 = "J4_img/agujero_sintopo.png";
                Imagen6 = "J4_img/agujero_sintopo.png";
                Imagen7 = "J4_img/agujero_sintopo.png";
                Imagen8 = "J4_img/agujero_sintopo.png";
                Imagen9 = "J4_img/agujero_sintopo.png";
                T1 = false; T2 = false; T3 = false; T4 = false; T5 = false; T6 = false; T7 = false; T8 = false; T9 = false;
                incremento = 0;

            }

        }

        public async void J4_EndGame()
        {
            if (MainPageViewModel.playedGames == 5)
            {
                PopupNavigation.Instance.PopAsync();
                MainPageViewModel h = new MainPageViewModel("", Navigation);
                h.PopUp();


            }
            else
            {
                PopupNavigation.Instance.PopAsync();
                await Navigation.PushModalAsync(new MainPage());
            }
        }

        private async void PopUp()
        {
            MainPageViewModel.puntuacionTotal += Contador1;
            MainPageViewModel.playedGames++;
            TxtPuntos = "PUNTUACIÓN: " + Contador1 + " PUNTOS";
            var pr = new PopUp(TxtPuntos, Navigation,0);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left

            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);


        }
    }
}