﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace WorldMiniGamesV1.ViewModel
{
    class EgyptPageViewModel : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public INavigation Navigation { get; set; }
        //COMANDOS 
        public ICommand J3_comandoVital { get; set; }
        public ICommand CerrarApp { get; set; }

        //VARIABLES
        public int J3_posicionBurbujaX = 0;
        public int J3_contadorBurbuja = 0;
        public int J3_ContadorMuerte = 0;
        public int J3_movimientoFuego;
        public int J3_contador = 0;
        public int J3_marcador = 0;
        public int J3_random = 0;
        public bool J3_visFuego = false;
        public bool J3_visFuegoEnem = false;
        public bool J3_visFuego1 = false;
        public bool J3_visibilidadBurbuja = false;
        public String J3_vida = "J3_img/J3_vida3m.png";
        public String J3_vidaEnemigo = "J3_img/J3_vida3m.png";
        public String J3_dios = "J3_img/anubis.png";
        public String J3_nombreEnemigo = "Anubis";
        public String imagenPopup = "J3_img/Faraon.png";
        public String j3_TxtPuntos;

        public bool finJuego = false;

        Random r = new Random();

        public int J3_posicionBurbujaY = 0;
        public int J3_PosicionBurbujaX
        {

            set { if (J3_posicionBurbujaX != value) { J3_posicionBurbujaX = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_PosicionBurbujaX")); } } }
            get
            {

                return J3_posicionBurbujaX;
            }
        }
        public int J3_MovimientoFuego
        {

            set { if (J3_movimientoFuego != value) { J3_movimientoFuego = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_MovimientoFuego")); } } }
            get
            {

                return J3_movimientoFuego;
            }
        }
        public int J3_Contador
        {

            set { if (J3_contador != value) { J3_contador = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_Contador")); } } }
            get
            {

                return J3_contador;
            }
        }
        public int J3_PosicionBurbujaY
        {

            set { if (J3_posicionBurbujaY != value) { J3_posicionBurbujaY = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_PosicionBurbujaY")); } } }
            get
            {

                return J3_posicionBurbujaY;
            }
        }

        public bool J3_VisibilidadBurbuja
        {

            set { if (J3_visibilidadBurbuja != value) { J3_visibilidadBurbuja = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_VisibilidadBurbuja")); } } }
            get
            {

                return J3_visibilidadBurbuja;
            }
        }
        public bool J3_VisFuego
        {

            set { if (J3_visFuego != value) { J3_visFuego = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_VisFuego")); } } }
            get
            {

                return J3_visFuego;
            }
        }
        public bool J3_VisFuego1
        {

            set { if (J3_visFuego1 != value) { J3_visFuego1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_VisFuego1")); } } }
            get
            {

                return J3_visFuego1;
            }
        }
        public bool J3_visFuegoEnem1 = false;
        public bool J3_VisFuegoEnem
        {

            set { if (J3_visFuegoEnem != value) { J3_visFuegoEnem = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_VisFuegoEnem")); } } }
            get
            {

                return J3_visFuegoEnem;
            }
        }
        public bool J3_VisFuegoEnem1
        {

            set { if (J3_visFuegoEnem1 != value) { J3_visFuegoEnem1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_VisFuegoEnem1")); } } }
            get
            {

                return J3_visFuegoEnem1;
            }
        }
        
        public String J3_Vida
        {

            set { if (J3_vida != value) { J3_vida = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_Vida")); } } }
            get
            {

                return J3_vida;
            }
        }
        public String J3_VidaEnemigo
        {

            set { if (J3_vidaEnemigo != value) { J3_vidaEnemigo = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_VidaEnemigo")); } } }
            get
            {

                return J3_vidaEnemigo;
            }
        }
        public String J3_Dios
        {

            set { if (J3_dios != value) { J3_dios = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_Dios")); } } }
            get
            {

                return J3_dios;
            }
        }
        public String J3_NombreEnemigo
        {

            set { if (J3_nombreEnemigo != value) { J3_nombreEnemigo = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J3_NombreEnemigo")); } } }
            get
            {

                return J3_nombreEnemigo;
            }
        }
        public String TxtPuntos
        {
            set
            {
                if (j3_TxtPuntos != value)
                {
                    j3_TxtPuntos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J3_TxtPuntos"));
                    }
                }
            }
            get
            {
                return j3_TxtPuntos;
            }
        }
        public String ImagenPopup
        {
            set 
            { if (imagenPopup != value) 
                { 
                    imagenPopup = value; if (PropertyChanged != null) 
                    { 
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup")); 
                    } 
                } 
            }
            get
            {
                return imagenPopup;
            }
        }


        public EgyptPageViewModel(String j3_TxtPuntos, INavigation navigation)
        {
            this.Navigation = navigation;
            this.TxtPuntos = j3_TxtPuntos;
            J3_comandoVital = new Command(J3_comandoBurbuja);
            J3_comprobacionVida();
            J3_animacionBurbuja();
            CerrarApp = new Command(J3_EndGame);
            ComprobarFin();


        }
        async void J3_comprobacionVida()
        {
            while (true)
            {
                if (J3_ContadorMuerte == 1)
                {
                    await Task.Delay(1000);
                    J3_Vida = "J3_img/J3_vida2m.png";
                }
                else if (J3_ContadorMuerte == 2)
                {
                    J3_Vida = "J3_img/J3_vida1m.png";
                    finJuego = true;

                }
                else if (J3_ContadorMuerte == 3)
                {
                    J3_Vida = "J3_img/J3_vida0m.png";
                    
                }
                await Task.Delay(300);
            }
        }
        async void J3_comandoBurbuja()
        {
            J3_Contador++;
            if (J3_Contador == 4)
            {
                await Task.Delay(3000);
                J3_VidaEnemigo = "J3_img/J3_vida0m.png";
                await Task.Delay(500);
                J3_VidaEnemigo = "J3_img/J3_vida3m.png";
                J3_NombreEnemigo = "Amon";
                J3_Dios = "J3_img/Amon.png";
            }

            else if (J3_Contador == 8)
            {
                await Task.Delay(3000);
                J3_VidaEnemigo = "J3_img/J3_vida0m.png";
                await Task.Delay(500);
                J3_VidaEnemigo = "J3_img/J3_vida3m.png";
                J3_NombreEnemigo = "Faraon";
                J3_Dios = "J3_img/Faraon.png";
            }
            else if (J3_Contador == 12)
            {
                await Task.Delay(3000);
                J3_VidaEnemigo = "J3_img/J3_vida0m.png";
                await Task.Delay(500);
                J3_VidaEnemigo = "J3_img/J3_vida3m.png";
                J3_NombreEnemigo = "Deidades";
                J3_Dios = "J3_img/Deidades.png";
            }
            else if (J3_Contador == 16)
            {
                await Task.Delay(3000);
                J3_VidaEnemigo = "J3_img/J3_vida0m.png";
                await Task.Delay(500);
                J3_VidaEnemigo = "J3_img/J3_vida3m.png";
                J3_NombreEnemigo = "Nefertiti";
                J3_Dios = "J3_img/Nefertiti.png";
            }
            else if (J3_Contador == 20)
            {
                await Task.Delay(3000);
                J3_VidaEnemigo = "J3_img/J3_vida0m.png";
                await Task.Delay(500);
                J3_VidaEnemigo = "J3_img/J3_vida3m.png";
                J3_NombreEnemigo = "Thoth";
                J3_Dios = "J3_img/Thoth.png";
            }else if (J3_Contador == 24)
            {
                finJuego = true;
            }
        }
        private async void PopUp()
        {
            MainPageViewModel.puntuacionTotal += J3_contador;
            MainPageViewModel.playedGames++;
            TxtPuntos = "PUNTUACIÓN: " + J3_Contador + " PUNTOS";
            var pr = new PopUp(TxtPuntos, Navigation, 0.1);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left

            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);


        }
        async void J3_animacionAtaque1()
        {
            J3_MovimientoFuego = 125;
            J3_VisFuego = true;
            await Task.Delay(1000);
            while (J3_MovimientoFuego < 360)
            {
             
                J3_VisFuego = false;
                J3_VisFuego1 = true;
                J3_MovimientoFuego = J3_MovimientoFuego + 150;
                await Task.Delay(10);
            }
            J3_VisFuego1 = false;

            J3_MovimientoFuego = 125;

        }
        async void J3_animacionAtaque2()
        {
            J3_MovimientoFuego = 320;
            J3_VisFuegoEnem = true;
            await Task.Delay(1000);
            J3_VisFuegoEnem = false;
            J3_VisFuegoEnem1 = true;
            while (J3_MovimientoFuego > 125)
            { 
                J3_MovimientoFuego = J3_MovimientoFuego - 150;
                await Task.Delay(10);
            }
            J3_VisFuegoEnem1 = false;
            J3_MovimientoFuego = 125;

        }
        async void J3_animacionBurbuja()
        {
            if (J3_ContadorMuerte == 2)
            {
                //J3_PopUp();
            }
            else
            {

            await Task.Delay(3000);
            int[] ejeX = new int[10];
            int[] ejeY = new int[10];
                //ejeX posiciones             
                ejeX[0] = 50;
                ejeX[1] = 100;
                ejeX[2] = 250;
                ejeX[3] = 300;
                ejeX[4] = 350;
                ejeX[5] = 239;
                ejeX[6] = 289;
                ejeX[7] = 170;
                ejeX[8] = 300;
                ejeX[9] = 200;

                //ejeY posiciones
                ejeY[0] = 800;
                ejeY[1] = 400;
                ejeY[2] = 450;
                ejeY[3] = 450;
                ejeY[4] = 300;
                ejeY[5] = 350;
                ejeY[6] = 600;
                ejeY[7] = 650;
                ejeY[8] = 700;
                ejeY[9] = 750;

                //RANDOM BURBUJA PRIMERA
                J3_contadorBurbuja = 0;
            while (J3_contadorBurbuja < 4)
            {
                J3_VisibilidadBurbuja = true;
                J3_random = r.Next(10);
                J3_PosicionBurbujaX = ejeX[J3_random];
                J3_random = r.Next(10);
                J3_PosicionBurbujaY = ejeY[J3_random];
                await Task.Delay(2000);
                J3_VisibilidadBurbuja = false;
                await Task.Delay(1000);
                J3_contadorBurbuja++;
            }
            J3_burbujaAleatoria();
            }
        }
        public void J3_burbujaAleatoria()
        {
            if (J3_ContadorMuerte == 2)
            {
                //J3_PopUp();
            }
            
            //COMPROBACION SI HAS HECHO 4 ACIERTOS
            if (J3_Contador < 4 && J3_marcador ==0)
            {
                J3_animacionAtaque2();
                J3_contadorBurbuja = 0;
                J3_Contador = 0;
                J3_ContadorMuerte++;
                J3_animacionBurbuja();
            }
            else if (J3_Contador < 8 && J3_marcador == 4)
            {
                J3_animacionAtaque2();
                J3_contadorBurbuja = 4;
                J3_Contador = 4;
                J3_ContadorMuerte++;
                J3_animacionBurbuja();

            }
            else if (J3_Contador < 12 && J3_marcador == 8)
            {
                J3_animacionAtaque2();
                J3_contadorBurbuja = 8;
                J3_Contador = 8;
                J3_ContadorMuerte++;
                J3_animacionBurbuja();

            }
            else if (J3_Contador < 16 && J3_marcador == 12)
            {
                J3_animacionAtaque2();
                J3_contadorBurbuja = 12;
                J3_Contador = 12;
                J3_ContadorMuerte++;
                J3_animacionBurbuja();

            }
            else if (J3_Contador < 20 && J3_marcador == 16)
            {
                J3_animacionAtaque2();
                J3_contadorBurbuja = 16;
                J3_Contador = 16;
                J3_ContadorMuerte++;
                J3_animacionBurbuja();

            }
            //APARTIR DE AQUI SE SIGUE ADELANTE
            else if (J3_Contador == 4)
            {
                J3_animacionAtaque1();
                J3_marcador = 4;         
                J3_animacionBurbuja();
            }
            //COMPROBACION SI HAS HECHO 8 SEGUIDAS
           
            //SI HAS HECHO LAS 8 SE SIGUE ADELANTE
            else if (J3_Contador == 8 )
            {
                J3_marcador = 8;
                //ATAQUE ALIADO
                J3_animacionAtaque1();
                //FIN ATAQUE ALIADO
               
                J3_animacionBurbuja();
              
            }
            //COMPROBACION SI HAS HECHO LAS 13 SEGUIDAS
          
            //SI LO HAS HECHO SEGUIMOS ADELANTE
            else if (J3_Contador == 12 )
            {
                J3_marcador = 12;
                J3_animacionAtaque1();
                
                J3_animacionBurbuja();
            }
            //COMPROBACIONES SI HAS HECHO LAS 18 SEGUIDAS
           
            else if (J3_Contador == 16 )
            {
                J3_marcador = 16;
                J3_animacionAtaque1();
                
                J3_animacionBurbuja();
            }
            //Sergio pa' que dejas este suelto TT

            else if (J3_Contador == 20)
            {
                J3_marcador = 20;
                J3_animacionAtaque1();

                J3_animacionBurbuja();
            }

            else if (J3_Contador == 24)
            {
                J3_marcador = 24;
                //Pop up
            }

        }

        public async void ComprobarFin()
        {
            while (finJuego == false)
            {
                await Task.Delay(1000);
            }

            if(MainPageViewModel.bugEgypt == 0)
            {
                MainPageViewModel.bugEgypt = 1;
                PopUp();
            }

            

        }

        public async void J3_EndGame()
        {
            if (MainPageViewModel.playedGames == 5)
            {
                PopupNavigation.Instance.PopAsync();
                MainPageViewModel h = new MainPageViewModel("", Navigation);
                h.PopUp();


            }
            else
            {
                PopupNavigation.Instance.PopAsync();
                await Navigation.PushModalAsync(new MainPage());
            }
        }
    }
}

