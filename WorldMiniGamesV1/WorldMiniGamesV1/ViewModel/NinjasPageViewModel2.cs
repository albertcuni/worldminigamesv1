﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using WorldMiniGamesV1.View;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;


namespace WorldMiniGamesV1
{
    public class NinjasPageViewModel2 : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public INavigation Navigation { get; set; }

        //comandos del programa
        public ICommand ComandoClick { get; set; }
        public ICommand ComandoClickGalleta { get; set; }
        public ICommand ComandoPosicionRandom { get; set; }
        public ICommand ComandoMultBonus { get; set; }

        public ICommand CerrarApp { get; set; }

        //variables de tipo entero
        public int puntuacion;
        public int timeleft = 30;//variable iniciada a 30 segundos que es el tiempo que dura la app
        //variablesd de tipo string
        public String personaje= "J7_img/ninja1.png";
        public String estrella = "J7_img/estrella1.png";
        public String personatje= "J7_img/ninja1.png";//variable para poder comparar el personaje 
        public String txtPuntos;
        public String imagenPopup = "J7_img/dragondorado.PNG";

        public int bonus = 0;
        Random r = new Random();
        public int randomX = 0;
        public int randomY = 0;
        public Boolean bool1 = false;
        public int aux2;
        public bool clickHilo = false;


        //variable que bindea la puntuación al xaml
        public int Puntuacion { set { if (puntuacion != value) { puntuacion = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Puntuacion")); } } } get { return puntuacion; } }
        //variable que bindea el personaje aleatorio al xaml
        public String Personaje { set { if (personaje != value) { personaje = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Personaje")); } } } get { return personaje; } }
        //variable que bindea el shuriken aleatorio al xaml
        public String Estrella { set { if (estrella != value) { estrella = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Estrella")); } } } get { return estrella; } }
        //variable que bindea el tiempo al xaml
        public int Timeleft { set { if (timeleft != value) { timeleft = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Timeleft")); } } } get { return timeleft; } }
        public int RandomX { set { if (randomX != value) { randomX = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("RandomX")); } } } get { return randomX; } }
        public int RandomY { set { if (randomY != value) { randomY = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("RandomY")); } } } get { return randomY; } }
        public Boolean Bool1 { set { if (bool1 != value) { bool1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Bool1")); } } } get { return bool1; } }
        public String TxtPuntos { set { if (txtPuntos != value) { txtPuntos = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("TxtPuntos")); } } } get { return txtPuntos;} }

        public String ImagenPopup
        {
            set { if (imagenPopup != value) { imagenPopup = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup")); } } }
            get
            {

                return imagenPopup;
            }
        }

        public NinjasPageViewModel2()
        {
            ComandoClick = new Command(click);
            ComandoClickGalleta = new Command(ClickGalleta);
            ComandoPosicionRandom = new Command(Random);
            ComandoMultBonus = new Command(MultBonus);
            CerrarApp = new Command(J7_EndGame);
            Random();
            Puntuacion = 0;
            
            
        }

        

        public NinjasPageViewModel2(String j7_TxtPuntos, INavigation navigation)
        {
            this.TxtPuntos = j7_TxtPuntos;
            this.Navigation = navigation;

            ComandoClick = new Command(click);
            ComandoClickGalleta = new Command(ClickGalleta);
            ComandoPosicionRandom = new Command(Random);
            ComandoMultBonus = new Command(MultBonus);
            CerrarApp = new Command(J7_EndGame);
            Random();
            Puntuacion = 0;
           


        }
        private void MultBonus(object obj)
        {

            try 
            {

                Bonus();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task Bonus()
        {
            Timeleft = Timeleft + 5;
            for (int x = 0; x <= 20; x++)
            {
                Bool1 = false;
                await Task.Delay(1000);
            }

        }
        private async Task Visibilidad()
        {
            while (true)
            {
                Bool1 = false;
                aux2 = r.Next(0, 5);
                if (aux2 == 4)
                {

                    Bool1 = true;

                }
                await Task.Delay(3000);
            }
        }

        private void Random()
        {
            try
            {

                Visibilidad();

                PosicionRandom();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async Task PosicionRandom()
        {

            while (true)
            {
                RandomX = r.Next(-10, 100);
                RandomY = r.Next(-800, 0);
                await Task.Delay(3000);
            }
        }
     
        private void ClickGalleta()//metodo que se ejecuta cuando clicas la galleta de la fortuna
        {
            int punt = 0; //variable para incrementar la puntuación
           
            char[] comparacion = new char[Personaje.Length]; //array de char donde separa la string del personaje
            comparacion = Personaje.ToCharArray();//introduce la string personaje convertida en char en el array
            if (comparacion[7].Equals('b'))//los personajes buenos empiezan todos por b. si empieza por b y clicas la galleta
            {

                punt = 1;//en el incremento se pone 1
                //soundCorrect();
            }
            else
            {
                if (puntuacion>0)//si la puntuacion es mayor de 0 
                {
                    punt = -1;//en el incremento se pone -1
                    //soundWrong();
                }
                
            }
            

            Puntuacion = puntuacion + punt; //la puntuación será siempre la puntuación + el incremento. si es uno suma un punto y si es menos uno resta un punto
            Personaje = ninjaAleatorio();//al finalizar el metodo se aplica el metodo ninjaAleatorio a personaje para cambiar el personaje de manera aleatoria
        }

        public void click()//metodo que se ejecuta cuando clicas el shuriken
        {
            if(clickHilo == false)
            {
                timer1_tick();
                clickHilo = true;
            }
            
            int punt = 0; //variable para incrementar la puntuación

            char[] comparacion = new char[Personaje.Length]; //array de char donde separa la string del personaje
            comparacion = Personaje.ToCharArray(); //introduce la string personaje convertida en char en el array

            if (comparacion[7].Equals('n')) //los personajes ninjas empiezan todos por n. si empieza por n y clicas el shuriken
            {

                punt = 1; //en el incremento se pone 1
                //soundCorrect();
            }

            else
            {
                if (puntuacion > 0)//si la puntuacion es mayor de 0 
                {
                    punt = -1;//en el incremento se pone -1
                    //soundWrong();
                }
            }
            

            
            Puntuacion = puntuacion + punt; //la puntuación será siempre la puntuación + el incremento. si es uno suma un punto y si es menos uno resta un punto
            Personaje =ninjaAleatorio();//al finalizar el metodo se aplica el metodo ninjaAleatorio a personaje para cambiar el personaje de manera aleatoria
            Estrella = estrellaAleatoria();// en este caso tambien se aplica el metodo estrellaAleatoria para cambiar de manera aleatoria el shuriken
        }

        private String ninjaAleatorio()//metodo que cambia el personaje aleatorio
        {
          
            Random r = new Random();

            int bandera = r.Next(2);//variable que es un numero random 0 o 1
            int rand=0; //variable para hacer random la imagen aleatoria

            String[] aleatorio = new string[2];
            
          if (Personaje.Equals(personatje))
            {
                if (bandera == 0)
                {
                    rand = r.Next(1, 31);
                    personatje = "J7_img/ninja" + rand + ".png";
                    aleatorio[bandera] = personatje;
                }
                if (bandera == 1)
                {
                    rand = r.Next(1, 7);
                    personatje = "J7_img/bueno" + rand + ".png";
                    aleatorio[bandera] = personatje;
                }
            }
            
            return aleatorio[bandera];
        }       

        private String estrellaAleatoria()//hace la estrella aleatoria
        {
            int rand = 0;
            String shuriken;
            rand = r.Next(1, 7);
            shuriken = "J7_img/estrella" + rand + ".png";


            if (Puntuacion == 10 || Puntuacion > 10)
            {
                rand = r.Next(1, 6);
                shuriken = "J7_img/estrelladorada" + rand + ".png";


                char[] comparacion = new char[Personaje.Length];
                comparacion = Personaje.ToCharArray();

                if (comparacion[7].Equals('n')) 
                {

                    Puntuacion = Puntuacion + 1;
                    //soundCorrect();
                }
                else
                {
                    Puntuacion = Puntuacion - 1;
                    //soundWrong();
                }
            } else if (Puntuacion < 10)
                {
                rand = r.Next(1, 7);
                shuriken = "J7_img/estrella" + rand + ".png";
            }
                

            return shuriken;
        }

       public async Task timer1_tick()//hace el temporizador
        {
            while (Timeleft > 0)
            {

                Timeleft = Timeleft - 1;
                await Task.Delay(1000);

            }
            // System.Environment.Exit(1);//cierrar el programa para finalizarlo.
            PopUp();
            //Timeleft = 30;
            //Puntuacion = 0;
            //Personaje = "J7_img/ninja1.png";
            //estrella = "J7_img/estrella1.png";
            //personatje = "J7_img/ninja1.png";
            //await timer1_tick();
            
        }

        private async void PopUp()
        {
            MainPageViewModel.puntuacionTotal += Puntuacion;
            MainPageViewModel.playedGames++;
            TxtPuntos = "PUNTUACIÓN: " + Puntuacion + " PUNTOS";
            var pr = new PopUp(TxtPuntos, Navigation,null);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left

            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);


        }

        public async void J7_EndGame()
        {
            if (MainPageViewModel.playedGames == 5)
            {
                PopupNavigation.Instance.PopAsync();
                MainPageViewModel h = new MainPageViewModel("", Navigation);
                h.PopUp();


            }
            else
            {
                PopupNavigation.Instance.PopAsync();
                await Navigation.PushModalAsync(new MainPage());
            }
        }

        //public void soundCorrect()
        //{
        //    var assembly1 = typeof(App).GetTypeInfo().Assembly;
        //    Stream audioStream1 = assembly1.GetManifestResourceStream("WorldMiniGamesV1.Sounds.correct.mp3");
        //    var audio1 = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
        //    audio1.Load(audioStream1);
        //    audio1.Play();

        //}

        //public void soundWrong()
        //{
        //    var assebmly2 = typeof(App).GetTypeInfo().Assembly;
        //    Stream audioStream2 = assebmly2.GetManifestResourceStream("WorldMiniGamesV1.Sounds.wrong.mp3");
        //    var audio2 = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
        //    audio2.Load(audioStream2);
        //    audio2.Play();
        //}


    }


}