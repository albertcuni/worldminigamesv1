﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{ 
    public class SumaPageViewModel : NavigationPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public INavigation Navigation { get; set; }
        // public ICommand Reset { get; set; } //Icommant del botor reset
        public ICommand Pasar { get; set; }//Icommant del botor Pasar
        public ICommand BotoOpcio1 { get; set; }//Icommant del botor BotoOpcio1
        public ICommand BotoOpcio2 { get; set; }//Icommant del botor BotoOpcio2
        public ICommand BotoOpcio3 { get; set; }//Icommant del botor BotoOpcio3
        public ICommand BotoOpcio4 { get; set; }//Icommant del botor BotoOpcio4
        public ICommand IniciJoc { get; set; }//Icommant del botor IniciJoc
        public ICommand FinalJoc { get; set; }//Icommant del botor FinalJoc
        public ICommand CerrarApp { get; set; }

        ContentPage nav; //Declarem per a poder mourens de pagina

        Random rand = new Random(); //Declara Random 

        public int Numero1
        {
            set
            {
                if (numero1 != value) { numero1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Numero1")); } }
            }
            get { return numero1; }
        }//Get i set

        public int numero1;//Variable

        public int Numero2
        {
            set
            {
                if (numero2 != value) { numero2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Numero2")); } }
            }
            get { return numero2; }
        }//Get i set

        public int numero2;//Variable

        public int Resultat
        {
            set
            {
                if (resultat != value) { resultat = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Resultat")); } }
            }
            get { return resultat; }
        } //Get i set

        public int resultat; //Variable

        public int PossibleResultat1
        {
            set
            {
                if (possibleResultat1 != value) { possibleResultat1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PossibleResultat1")); } }
            }
            get { return possibleResultat1; }
        } //Get i set

        public int possibleResultat1; //Variable

        public int PossibleResultat2
        {
            set
            {
                if (possibleResultat2 != value) { possibleResultat2 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PossibleResultat2")); } }
            }
            get { return possibleResultat2; }
        } //Get i set

        public int possibleResultat2; //Variable

        public int PossibleResultat3
        {
            set
            {
                if (possibleResultat3 != value) { possibleResultat3 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("PossibleResultat3")); } }
            }
            get { return possibleResultat3; }
        } //Get i set

        public int possibleResultat3; //Variable 

        public int Punts
        {
            set
            {
                if (punts != value) { punts = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Punts")); } }
            }
            get { return punts; }
        }//Get i set

        public int punts = 0; //Variable

        public int suma = 1; //Ints per fer random de si es suma resta ...
        public int resta = 2;
        public int divisio = 3;
        public int multiplicacio = 4;

        public int operador; //Random per saber si es suma resta ...

        public int contOk;

        public String Operadors
        {
            set
            {
                if (operadors != value) { operadors = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Operadors")); } }
            }
            get { return operadors; }
        } //Get i set
        public String operadors; // Guardo els simbols de les operacions
        public String txtPuntos;
        public String imagenPopup = "J9_img/mate.png";

        int[] resultats = new int[4]; //Inicialitzem l'array per guardar numeros
        int[] resultatsDesordenats = new int[4]; //Inicialitzem l'array per desordenar

        public int ResultatBo
        {
            set
            {
                if (resultatBo != value) { resultatBo = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("ResultatBo")); } }
            }
            get { return resultatBo; }
        } //Get i set
        int resultatBo = 0;

        public int Segons
        {
            set
            {
                if (segons != value) { segons = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Segons")); } }
            }
            get { return segons; }
        } //Get i set
        int segons;//Inicialitzem els segons

        Boolean autocont = true;//inicialitzem el boolean 

        public String Fons
        {
            set
            {
                if (fons != value) { fons = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Fons")); } }
            }
            get { return fons; }
        } //Get i set
        String fons = "J9_img/MainPage.png"; //Bindejem el fons de pantalla

        public String Visible
        {
            set
            {
                if (visible != value) { visible = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Visible")); } }
            }
            get { return visible; }
        } //Get i set
        String visible = "False"; //Binding boolean per oculta text

        public String ImagenPopup
        {
            set
            {
                if (imagenPopup != value)
                {
                    imagenPopup = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup"));
                    }
                }
            }
            get
            {
                return imagenPopup;
            }
        }
        public String TxtPuntos
        {
            set
            {
                if (txtPuntos != value)
                {
                    txtPuntos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TxtPuntos"));
                    }
                }
            }
            get
            {
                return txtPuntos;
            }
        }



        public SumaPageViewModel(String txtPuntos, INavigation navigation, ContentPage navi)
        {
            this.Navigation = navigation;
            this.TxtPuntos = txtPuntos;
            nav = navi;//serveix per poder navegar entre pagines
            //a = 0;
            Segons = 31; //Inicialitzem els segons
            OperacionsJocAsync();//Accedim a la funcio
            TemporitzadorAsync();//Accedim a la funcio
            //Reset = new Command(ResetJoc); //Al clicar anem a la funcio
            Pasar = new Command(Passar); //Al clicar anem a la funcio
            BotoOpcio1 = new Command(FuncioOpcio1); //Al clicar anem a la funcio
            BotoOpcio2 = new Command(FuncioOpcio2); //Al clicar anem a la funcio
            BotoOpcio3 = new Command(FuncioOpcio3); //Al clicar anem a la funcio
            BotoOpcio4 = new Command(FuncioOpcio4); //Al clicar anem a la funcio
            //IniciJoc = new Command(ComandoInici); //Al clicar anem a la funcio
            //FinalJoc = new Command(ComandoFijoc); //Al clicar anem a la funcio
            CerrarApp = new Command(J9_EndGame);
        }

        public void Passar() { //Passa el torn 
            Punts = Punts - 1; //Resem un punt al clicar.
            OperacionsJocAsync();//Tornem a cridar la funcio 
            Vibracio();//Accedim a la funcio
            SoPassar();
        }

        //public void ResetJoc() //Reinicialitzem totes les variables a 0 i reiniciem les funcions.
        //{
        //    Vibracio();
        //    autocont = false;
        //    Resultat = 0;
        //    Numero1 = 0;
        //    Numero2 = 0;
        //    Punts = 0;
        //    Segons = 30;
        //    PossibleResultat1 = 0;
        //    PossibleResultat2 = 0;
        //    PossibleResultat3 = 0;
        //    autocont = true;
        //    OperacionsJocAsync();
        //}

        public async Task OperacionsJocAsync()
        {
            await Task.Delay(1000);//Delay 1 segon
            Fons = "J9_img/MainPage.png";//Canviem el background
     
            operador = rand.Next(1, 5); //Random operador
            Numero1 = rand.Next(0, 99); //Random numero 1 per la suma,resta i multiplicacio
            Numero2 = rand.Next(0, 99); //Random numero 2 per la suma,resta i multiplicacio

            if (operador == suma) //Si el random es igual al numero assignat a l'operacio accedirem al if
            {
                Resultat = Numero1 + Numero2; //Fem l'operacio i emmagatzem el resultat a una variable
                Operadors = "+"; //Passem el simbol de l'operacio per que o veigui el usuari.
            }
            if (operador == resta)//Si el random es igual al numero assignat a l'operacio accedirem al if
            {
                Resultat = Numero1 - Numero2;//Fem l'operacio i emmagatzem el resultat a una variable
                Operadors = "-"; //Passem el simbol de l'operacio per que o veigui el usuari.
            }
            if (operador == multiplicacio)//Si el random es igual al numero assignat a l'operacio accedirem al if
            {
                Numero1 = rand.Next(0, 20); //Random numero 1 per la suma,resta i multiplicacio
                Numero2 = rand.Next(0, 10); //Random numero 2 per la suma,resta i multiplicacio
                Resultat = Numero1 * Numero2;//Fem l'operacio i emmagatzem el resultat a una variable
                Operadors = "*"; //Passem el simbol de l'operacio per que o veigui el usuari.
            }
            if (operador == divisio)//Si el random es igual al numero assignat a l'operacio accedirem al if
            {
                Numero1 = rand.Next(1, 99); //Random numero 1 per la divicio
                Numero2 = rand.Next(1, 10); //Random numero 2 per la divicio
                Resultat = Numero1 / Numero2;//Fem l'operacio i emmagatzem el resultat a una variable
                Operadors = "/"; //Passem el simbol de l'operacio per que o veigui el usuari.
            }
            ResultatBo = Resultat;
            
            randomResultats();
        }

        public void randomResultats()
        {
            int resmes = Resultat + 20; //numero per fer el random del resultat
            int resmenys = Resultat - 20; //numero per fer el random del resultat

            do
            {
                PossibleResultat1 = rand.Next(resmenys, resmes);  //Random numero 1 per a que no es repeteixi el numero no para de ferse fincs que surt un de diferen
            } while (PossibleResultat1 == PossibleResultat2 || PossibleResultat1 == PossibleResultat3 || PossibleResultat1 == Resultat);
            do
            {
                PossibleResultat2 = rand.Next(resmenys, resmes);  //Random numero 2 per a que no es repeteixi el numero no para de ferse fincs que surt un de diferen
            } while (PossibleResultat2 == PossibleResultat1 || PossibleResultat2 == PossibleResultat3 || PossibleResultat2 == Resultat);
            do
            {
                PossibleResultat3 = rand.Next(resmenys, resmes);  //Random numero 3 per a que no es repeteixi el numero no para de ferse fincs que surt un de diferen
            } while (PossibleResultat3 == PossibleResultat2 || PossibleResultat3 == PossibleResultat1 || PossibleResultat3 == Resultat);


            //Guardem els numeros a un array per a despres desordenar l'array
            resultats[0] = Resultat; 
            resultats[1] = PossibleResultat1;
            resultats[2] = PossibleResultat2;
            resultats[3] = PossibleResultat3;

            //Random para desordenar l'array fins que l'array no estigui desordenada no parara de fer el while.
            List<int> listNumbers = new List<int>();
            int number;
            for (int i = 0; i < 4; i++)
            {
                do
                {
                    number = rand.Next(4);
                } while (listNumbers.Contains(number));
                listNumbers.Add(number);
            }

            Resultat = resultats[listNumbers[0]]; //Passem els numeros desordenats a la variable per mostrar 
            PossibleResultat1 = resultats[listNumbers[1]];//Passem els numeros desordenats a la variable per mostrar 
            PossibleResultat2 = resultats[listNumbers[2]];//Passem els numeros desordenats a la variable per mostrar 
            PossibleResultat3 = resultats[listNumbers[3]];//Passem els numeros desordenats a la variable per mostrar 

        }

        public void FuncioOpcio1() //Quan pitjem el boto 1 ejecutem la funcio
        {
            Vibracio(); //Accedim a la funcio
            if (Resultat == ResultatBo) //Si el resultatd es correcte sumba un punt i la pantalla s'ilumina de color verd
            {
                Punts = Punts + 1; //Suma un punt
                Fons = "J9_img/Ok.png"; //Canviem el background
                Correcto();
            }
            else //Si es errorni el fons s'ilumina de color vermell i restem 1 punt.
            {
                Punts = Punts -1; //Restem 1 punt
                Fons = "J9_img/Error.png"; //Canviem el fons de pantalla
                Visibles();//Accedim a la funcio.
                Incorrecto();
            }
            OperacionsJocAsync(); //Fem una altra ronda
        }

        public void FuncioOpcio2()//Quan pitjem el boto 1 ejecutem la funcio
        {
            Vibracio();//Accedim a la funcio
            if (PossibleResultat1 == ResultatBo)//Si el resultatd es correcte sumba un punt i la pantalla s'ilumina de color verd
            {
                Punts = Punts + 1;//Suma un punt
                Fons = "J9_img/Ok.png";//Canviem el background
                Correcto();
            }
            else //Si es errorni el fons s'ilumina de color vermell i restem 1 punt.
            {
                Punts = Punts -1;//Restem 1 punt
                Fons = "J9_img/Error.png"; //Canviem el background
                Visibles();//Accedim a la funcio.
                Incorrecto();
            }
            OperacionsJocAsync();
        }

        public void FuncioOpcio3()
        {
            Vibracio();//Accedim a la funcio
            if (PossibleResultat2 == ResultatBo)//Si el resultatd es correcte sumba un punt i la pantalla s'ilumina de color verd
            {
                Punts = Punts + 1;//Suma un punt
                Fons = "J9_img/Ok.png"; //Canviem el background
                Correcto();
            }
            else //Si es errorni el fons s'ilumina de color vermell i restem 1 punt.
            {
                Punts = Punts - 1;//Restem 1 punt
                Fons = "J9_img/Error.png"; //Canviem el background
                Visibles();//Accedim a la funcio.
                Incorrecto();
            }
            OperacionsJocAsync();
        }

        public void FuncioOpcio4()//Si el resultatd es correcte sumba un punt i la pantalla s'ilumina de color verd
        {
            Vibracio();//Accedim a la funcio
            if (PossibleResultat3 == ResultatBo)//Si el resultatd es correcte sumba un punt i la pantalla s'ilumina de color verd
            {
                Punts = Punts + 1;//Suma un punt
                Fons = "J9_img/Ok.png"; //Canviem el background
                Correcto();
            }
            else //Si es errorni el fons s'ilumina de color vermell i restem 1 punt.
            {
                Punts = Punts - 1;//Restem 1 punt
                Fons = "J9_img/Error.png"; //Canviem el background
                Visibles();//Accedim a la funcio.
                Incorrecto();
            }
            OperacionsJocAsync();
        }
        
        public async Task TemporitzadorAsync() //Temporitzador resta d'un en un els segons
        {
            
            while (autocont == true && Segons!= 0) //Si la variable es true el temporitzador funciona
            {
                Segons = Segons - 1; //Cada segon restara -1
                await Task.Delay(1000);// Donem un segon de delay
            
                if (Segons == 0) //Si els segons arriba a 0  passem el boolean a false
                {
                    autocont = false; //Passem el boolean a false

                    if (MainPageViewModel.bugSuma == 0)
                    {
                        MainPageViewModel.bugSuma = 1;
                        PopUp();
                    }

                }
              
            } 
        }

        //private async void ComandoFijoc()
        //{
        //    await nav.Navigation.PushModalAsync(new Inici()); //Un cop acabat el pitgem per tornar al inici per reiniciar el joc

        //}

        //private async void ComandoInici()
        //{
        //    await nav.Navigation.PushModalAsync(new MainPage()); //Accedir a la pagina per a puguer jugar
        //}



        public void Vibracio() //Aquesta funcio el que fa es fer vibrar el mobil.
        {
            try
            {
                // Use default vibration length
                Vibration.Vibrate();

                // Or use specified time
                var duration = TimeSpan.FromSeconds(0.1); //Duracio de la vibracio
                Vibration.Vibrate(duration);
            }
            catch (FeatureNotSupportedException ex)
            {
                // Feature not supported on device
            }
            catch (Exception ex)
            {
                // Other error has occurred.
            }
        }

        private async void Visibles()//Accedim a la funcio quan el usuari s'equivoqui
        {
            Visible = "True"; //Quan es True es mostrara per pantalla el missatge
            await Task.Delay(800);//Delay 800 milisegons
            Visible = "False";//Quan es False desapareixera per la panralla


        }

        public async void Correcto()
        {
            var correcto = typeof(MainPage).GetTypeInfo().Assembly;
            Stream auidoStream = correcto.GetManifestResourceStream("WorldMiniGamesV1.Sounds.ok.mp3");
            var audio = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
            audio.Load(auidoStream);
            audio.Play();
            await Task.Delay(1000);
            audio.Stop();
        }
        public async void Incorrecto()
        {
            var incorrecto = typeof(MainPage).GetTypeInfo().Assembly;
            Stream auidoStream = incorrecto.GetManifestResourceStream("WorldMiniGamesV1.Sounds.error.mp3");
            var audio = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
            audio.Load(auidoStream);
            audio.Play();
            await Task.Delay(1000);
            audio.Stop();
        }
        public async void SoPassar()
        {
            var pass = typeof(MainPage).GetTypeInfo().Assembly;
            Stream auidoStream = pass.GetManifestResourceStream("WorldMiniGamesV1.Sounds.pasar.mp3");
            var audio = Plugin.SimpleAudioPlayer.CrossSimpleAudioPlayer.Current;
            audio.Load(auidoStream);
            audio.Play();
            await Task.Delay(1000);
            audio.Stop();
        }

        private async void PopUp()
        {
            MainPageViewModel.puntuacionTotal += Punts;
            MainPageViewModel.playedGames++;
            Punts = Punts * 5;
            TxtPuntos = "PUNTUACIÓN: " + Punts + " PUNTOS";
            var pr = new PopUp(TxtPuntos,Navigation, 0,0);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left

            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);


        }

        public async void J9_EndGame()
        {
            if (MainPageViewModel.playedGames == 5)
            {
                PopupNavigation.Instance.PopAsync();
                MainPageViewModel h = new MainPageViewModel("", Navigation);
                h.PopUp();


            }
            else
            {
                PopupNavigation.Instance.PopAsync();
                await Navigation.PushModalAsync(new MainPage());
            }
        }

    }
}