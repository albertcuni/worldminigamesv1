﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorldMiniGamesV1.ViewModel
{
    public class Jugador
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public int Puntuacion { get; set; }
    }
}
