﻿using Rg.Plugins.Popup.Animations;
using Rg.Plugins.Popup.Enums;
using Rg.Plugins.Popup.Services;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    public class VasosPageViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public INavigation Navigation { get; set; }

        /*COMANDOS DECLARADOS*/
        public ICommand J12_ComandoCont { get; set; }
        public ICommand J12_ComandoIniciar { get; set; }
        public ICommand J12_ComandoMezclar { get; set; }
        public ICommand J12_ComandoOpcioA { get; set; }
        public ICommand J12_ComandoOpcioB { get; set; }
        public ICommand J12_ComandoOpcioC { get; set; }
        public ICommand CerrarApp { get; set; }



        public long j12_contador; //VARIABLE CONTADOR INCICIALIZADA

        //Int
        public int J12_punts = 0; //Variable
        public int j12_numeroRandom;
        public int j12_incremento = 0; //VARIABLE INCREMENTO INCICIALIZADA
        public int j12_cronometro;//VARIABLE CRONOMETRO INCICIALIZADA
        public int j12_resultat;
        public int j12_resultat_Correcta;

        //Strings
        public String j12_vaso1_IMG = "J6_img/j12_trans_vaso.png";
        public String j12_vaso2_IMG = "J6_img/j12_trans_vaso.png";
        public String j12_vaso3_IMG = "J6_img/j12_trans_vaso.png";
        public String j12_visibleBotons = "False";
        public String j12_visibleVolveraJugar = "False";
        public String j12_visibleStart = "True";

        public String imagenPopup = "J6_img/j12_trans_vaso_pelota.png";
        public String txtPuntos;

        public VasosPageViewModel(J12_Numero j12_num, String txtPuntos, INavigation navigation)
        {
            this.Navigation = navigation;
            this.TxtPuntos = txtPuntos;
            J12_ComandoIniciar = new Command(J12_IniciarPartida);
            J12_Resultat = j12_num.J12_Random;
            J12_ComandoMezclar = new Command(J12_DondeEstaLaBola); //TODOS LOS COMANDOS INICIALIZADOS
            J12_ComandoOpcioA = new Command(J12_ComprovacioA);
            J12_ComandoOpcioB = new Command(J12_ComprovacioB);
            J12_ComandoOpcioC = new Command(J12_ComprovacioC);
            CerrarApp = new Command(J6_EndGame);
        }

        //GETTERS Y SETTERS DE TODAS LA VARIABLES 
        public long J12_Contador
        {
            set
            {
                if (j12_contador != value)
                {
                    j12_contador = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_Contador"));
                    }
                }
            }
            get
            {
                return j12_contador;
            }
        }
        public int J12_Cronometro
        {
            set
            {
                if (j12_cronometro != value)
                {
                    j12_cronometro = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_Cronometro"));
                    }
                }
            }
            get
            {
                return j12_cronometro;
            }
        }

        //GETTERS Y SETERS SOURCE IMAGENES
        public String J12_Vaso1_IMG
        {
            set
            {
                if (j12_vaso1_IMG != value)
                {
                    j12_vaso1_IMG = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_Vaso1_IMG"));
                    }
                }
            }
            get
            {
                return j12_vaso1_IMG;
            }
        }
        public String J12_Vaso2_IMG
        {
            set
            {
                if (j12_vaso2_IMG != value)
                {
                    j12_vaso2_IMG = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_Vaso2_IMG"));
                    }
                }
            }
            get
            {
                return j12_vaso2_IMG;
            }
        }
        public String J12_Vaso3_IMG
        {
            set
            {
                if (j12_vaso3_IMG != value)
                {
                    j12_vaso3_IMG = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_Vaso3_IMG"));
                    }
                }
            }
            get
            {
                return j12_vaso3_IMG;
            }
        }
        public String J12_VisibleBotons
        {
            set
            {
                if (j12_visibleBotons != value)
                {
                    j12_visibleBotons = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_VisibleBotons"));
                    }
                }
            }
            get
            {
                return j12_visibleBotons;
            }
        }
        public String J12_VisibleVolveraJugar
        {
            set
            {
                if (j12_visibleVolveraJugar != value)
                {
                    j12_visibleVolveraJugar = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_VisibleVolveraJugar"));
                    }
                }
            }
            get
            {
                return j12_visibleVolveraJugar;
            }
        }
        public String J12_VisibleStart
        {
            set
            {
                if (j12_visibleStart != value)
                {
                    j12_visibleStart = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("J12_VisibleStart"));
                    }
                }
            }
            get
            {
                return j12_visibleStart;
            }
        }
        public String TxtPuntos
        {
            set
            {
                if (txtPuntos != value)
                {
                    txtPuntos = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("TxtPuntos"));
                    }
                }
            }
            get
            {
                return txtPuntos;
            }
        }
        public String ImagenPopup
        {
            set
            {
                if (imagenPopup != value)
                {
                    imagenPopup = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new PropertyChangedEventArgs("ImagenPopup"));
                    }
                }
            }
            get
            {
                return imagenPopup;
            }
        }

        public int J12_Punts
        {
            set
            {
                if (J12_punts != value) { J12_punts = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("J12_Punts")); } }
            }
            get { return J12_punts; }
        }//Get i set
        public int J12_Resultat
        {
            set
            {
                if (j12_resultat != value) { j12_resultat = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Resultat")); } }
            }
            get { return j12_resultat; }
        }//Get i set

        /*FUNCION PARA INICIAR LA PARTIDA*/
        public async void J12_IniciarPartida()
        {
            J12_VisibleStart = "False";
            J12_Cronometro = 31;
            J12_IniciarCronometro();
            await Task.Delay(750);
            J12_DondeEstaLaBola();

        }

        /*FUNCION DE INICIAR CRONOMETRO*/
        public async Task J12_IniciarCronometro()
        {
            do
            {
                J12_Cronometro--;
                await Task.Delay(1000);

            } while (J12_Cronometro != 0);

            if (J12_Cronometro == 0)
            {
                PopUp();
                J12_Cronometro = 0;
            }
        }

        public void J12_DondeEstaLaBola()
        {
            J12_VisibleVolveraJugar = "False"; 
            J12_VisibleBotons = "False";
            J12_mezclarBola();
        }
        public async void J12_mezclarBola()
        {          
            if (J12_Resultat == 1)
            {
                //await Task.Delay(100);
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso_pelota.png";
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso_sinpelota.png";
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso_sinpelota.png";
                await Task.Delay(500);
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso.png";
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso.png";
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso.png";
                j12_resultat_Correcta = 2;
            }
            else if (J12_Resultat == 2)
            {
                //await Task.Delay(100);
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso_pelota.png";
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso_sinpelota.png";
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso_sinpelota.png";
                await Task.Delay(500);
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso.png";
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso.png";
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso.png";
                j12_resultat_Correcta = 3;
            }
            else if (J12_Resultat == 3)
            {
                //await Task.Delay(100);
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso_pelota.png";
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso_sinpelota.png";
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso_sinpelota.png";
                await Task.Delay(500);
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso.png";
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso.png";
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso.png";
                j12_resultat_Correcta = 1;
            }
            await Task.Delay(5310);
            J12_VisibleBotons = "True";
            //comprovacio();
        }
        public void J12_ComprovacioA()
        {
            J12_OpcioA();
        }

        public async Task J12_OpcioA()
        {
            if (j12_resultat_Correcta == 1)
            {
                J12_Punts = J12_Punts + 1;
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso_pelota.png";
                await Task.Delay(1000);
                J12_Vaso3_IMG = "J6_img/j12_trans_vaso.png";
                J12_VisibleBotons = "False";
                J12_VisibleVolveraJugar = "True";
            }
            else
            {
                J12_Punts = J12_Punts - 1;
                J12_VisibleBotons = "False";
                J12_VisibleVolveraJugar = "True";
                if (j12_resultat_Correcta == 2)
                {
                    J12_Vaso1_IMG = "J6_img/j12_trans_vaso_pelota.png";
                    await Task.Delay(1000);
                    J12_Vaso1_IMG = "J6_img/j12_trans_vaso.png";
                    J12_VisibleBotons = "False";
                }
                if (j12_resultat_Correcta == 3)
                {
                    J12_Vaso2_IMG = "J6_img/j12_trans_vaso_pelota.png";
                    await Task.Delay(1000);
                    J12_Vaso2_IMG = "J6_img/j12_trans_vaso.png";
                    J12_VisibleBotons = "False";
                }

            }
            J12_VisibleVolveraJugar = "True";
        }
        
        public void J12_ComprovacioB()
        {
            J12_OpcioB();
        }

        public async Task J12_OpcioB()
        {
            if (j12_resultat_Correcta == 2)
            {
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso_pelota.png";
                await Task.Delay(1000);
                J12_Vaso1_IMG = "J6_img/j12_trans_vaso.png";
                J12_VisibleBotons = "False";
                J12_VisibleVolveraJugar = "True";
            }
            else
            {
                J12_Punts = J12_Punts - 1;
                J12_VisibleBotons = "False";
                J12_VisibleVolveraJugar = "True";

                if (j12_resultat_Correcta == 1)
                {
                    J12_Vaso3_IMG = "J6_img/j12_trans_vaso_pelota.png";
                    await Task.Delay(1000);
                    J12_Vaso3_IMG = "J6_img/j12_trans_vaso.png";
                    J12_VisibleBotons = "False";
                    
                }
                if (j12_resultat_Correcta == 3)
                {
                    J12_Vaso2_IMG = "J6_img/j12_trans_vaso_pelota.png";
                    await Task.Delay(1000);
                    J12_Vaso2_IMG = "J6_img/j12_trans_vaso.png";
                    J12_VisibleBotons = "False";
                }


            }
            J12_VisibleVolveraJugar = "True";
        }
        public void J12_ComprovacioC()
        {
            J12_OpcioC();
        }
        public async Task J12_OpcioC()
        {
            if (j12_resultat_Correcta == 3)
            {
                J12_Punts = J12_Punts + 1;
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso_pelota.png";
                await Task.Delay(1000);
                J12_Vaso2_IMG = "J6_img/j12_trans_vaso.png";
                J12_VisibleBotons = "False";
                J12_VisibleVolveraJugar = "True";
            }
            else
            {
                J12_Punts = J12_Punts - 1;
                J12_VisibleBotons = "False";
                J12_VisibleVolveraJugar = "True";

                if (j12_resultat_Correcta == 1)
                {
                    J12_Vaso3_IMG = "J6_img/j12_trans_vaso_pelota.png";
                    await Task.Delay(1000);
                    J12_Vaso3_IMG = "J6_img/j12_trans_vaso.png";
                    J12_VisibleBotons = "False";
                }
                if (j12_resultat_Correcta == 2)
                {
                    J12_Vaso1_IMG = "J6_img/j12_trans_vaso_pelota.png";
                    await Task.Delay(1000);
                    J12_Vaso1_IMG = "J6_img/j12_trans_vaso.png";
                    J12_VisibleBotons = "False";
                }
            }
            J12_VisibleVolveraJugar = "True";
        }

        private async void PopUp()
        {
            MainPageViewModel.puntuacionTotal += J12_Punts+10;
            MainPageViewModel.playedGames++;

            J12_Numero jnum = new J12_Numero();
            J12_Punts = J12_Punts * 10;
            TxtPuntos = "PUNTUACIÓN: " + J12_Punts + " PUNTOS";
            var pr = new PopUp(jnum,TxtPuntos, Navigation, 0.0f);

            var scaleAnimation = new ScaleAnimation
            {
                PositionIn = MoveAnimationOptions.Right,
                PositionOut = MoveAnimationOptions.Left

            };

            pr.Animation = scaleAnimation;
            pr.CloseWhenBackgroundIsClicked = false;
            await PopupNavigation.PushAsync(pr);

        }

        public async void J6_EndGame()
        {
            if (MainPageViewModel.playedGames == 5)
            {
                PopupNavigation.Instance.PopAsync();
                MainPageViewModel h = new MainPageViewModel("", Navigation);
                h.PopUp();

            }
            else
            {
                PopupNavigation.Instance.PopAsync();
                await Navigation.PushModalAsync(new MainPage());
            }
        }

    }
}