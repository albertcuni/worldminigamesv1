﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using WorldMiniGamesV1.ViewModel;

namespace WorldMiniGamesV1.Services
{
    class Controlador
    {
        static string cadenaConexion = @"Data Source=oracle.ilerna.com;Initial Catalog=DAM2_CUNILLERABENETALBERT_Clicker; User ID=DAM2_39528338D;Password=Albertcuni11*";
        public int IDs { get; set; }
        public string Nombres { get; set; }
        public int Puntuaciones { get; set; }

        public static List<Jugador> ObtenerJugadores()
        {
            List<Jugador> listaJugadores = new List<Jugador>();
            string sql = "SELECT * FROM RANKING ORDER BY puntuacio DESC";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();

                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Jugador jugador = new Jugador()
                            {
                                ID = reader.GetInt32(0),
                                Nombre = reader.GetString(1),
                                Puntuacion = reader.GetInt32(2),
                            };

                            listaJugadores.Add(jugador);
                        }
                    }
                }

                con.Close();

                return listaJugadores;
            }
        }

        public static string AgregarJugador(string nom, int puntos)
        {
            int x = 0;
            int idfinal = 0;
            string mensaje = "conexion intentada";

            string sql1 = "SELECT top 1 * FROM RANKING ORDER BY puntuacio DESC";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql1, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            idfinal = reader.GetInt32(0);
                        }
                    }
                }

                con.Close();
            }




            idfinal = idfinal + 1;

            string sql2 = "INSERT INTO RANKING  (id , nickname, puntuacio) VALUES (" + idfinal + ",'" + nom + "', " + puntos + ");";

            using (SqlConnection con = new SqlConnection(cadenaConexion))
            {
                con.Open();
                mensaje = "conexion establecida";
                using (SqlCommand comando = new SqlCommand(sql2, con))
                {
                    comando.ExecuteNonQuery();
                    mensaje = "comando ejecutado";
                }

                con.Close();
            }

            return mensaje;
        }
    }
}
