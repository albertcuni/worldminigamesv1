﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorldMiniGamesV1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TutorialVasos : ContentPage
    {
        public TutorialVasos()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel("", Navigation);
        }
    }
}