﻿using WorldMiniGamesV1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorldMiniGamesV1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUp2 
    {
        

        public PopUp2(int Combustible, INavigation nav)
        {
            InitializeComponent();
            BindingContext = new ClickerPageViewModel(1,Combustible,nav);
            
        }
               
    }
}