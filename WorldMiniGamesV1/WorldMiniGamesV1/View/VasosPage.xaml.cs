﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    [DesignTimeVisible(true)]
    public partial class VasosPage : ContentPage
    {
        public int cronometro = 31;
        public int j12_randomNumero;
        J12_Numero j12_num = new J12_Numero();

        public VasosPage()
        {

            InitializeComponent();
            J12_start();
            BindingContext = new VasosPageViewModel(j12_num, "", Navigation);
            
        }

        private void J12_start() {

            Random rd = new Random();
            j12_randomNumero = rd.Next(1, 3);
            j12_num.J12_Random = j12_randomNumero;
        }

        private async void J12_Mezclar_Vasos(object sender, EventArgs e)
        {
            J12_start();

            await Task.Delay(1000);

             if (j12_randomNumero == 1)
            {
                //ANIMACION BOLA EN EL VASO 1
                await Task.Delay(560);
                await j12_vaso1.TranslateTo(0, -50); 
                await j12_vaso2.TranslateTo(0, -50); 
                await j12_vaso3.TranslateTo(0, -50);
                await j12_vaso1.TranslateTo(220, -50); 
                await j12_vaso2.TranslateTo(220, -50); 
                await j12_vaso3.TranslateTo(-440, -50);
                await j12_vaso2.TranslateTo(0, -50); 
                await j12_vaso3.TranslateTo(0, -50);
                await j12_vaso1.TranslateTo(0, -50);
                await j12_vaso1.TranslateTo(440, -50);
                await j12_vaso2.TranslateTo(-220, -50);
                await j12_vaso3.TranslateTo(-220, -50);
                await Task.Delay(500);
                await j12_vaso1.TranslateTo(250, 0);
                await j12_vaso2.TranslateTo(-250, 0); 
                await j12_vaso3.TranslateTo(0, 0);
                //Resultat=2
            }
            else if (j12_randomNumero == 2)
            {
                //ANIMACION BOLA EN EL VASO 2
                await Task.Delay(560);
                await j12_vaso1.TranslateTo(0, -50); 
                await j12_vaso2.TranslateTo(0, -50); 
                await j12_vaso3.TranslateTo(0, -50);
                await j12_vaso1.TranslateTo(440, -50); 
                await j12_vaso2.TranslateTo(-220, -50);
                await j12_vaso3.TranslateTo(-220, -50);
                await j12_vaso3.TranslateTo(0, -50); 
                await j12_vaso1.TranslateTo(0, -50); 
                await j12_vaso2.TranslateTo(0, -50);
                await j12_vaso1.TranslateTo(220, -50); 
                await j12_vaso2.TranslateTo(220, -50); 
                await j12_vaso3.TranslateTo(-440, -50);
                await Task.Delay(500);
                await j12_vaso1.TranslateTo(0, 0); 
                await j12_vaso2.TranslateTo(250, 0); 
                await j12_vaso3.TranslateTo(-250, 0);
            }
            else if (j12_randomNumero == 3)
            {
                //ANIMACION BOLA EN EL VASO 3
                await Task.Delay(560);
                await j12_vaso1.TranslateTo(0, -50);
                await j12_vaso2.TranslateTo(0, -50); 
                await j12_vaso3.TranslateTo(0, -50);
                await j12_vaso3.TranslateTo(-440, -50); 
                await j12_vaso1.TranslateTo(220, -50); 
                await j12_vaso2.TranslateTo(220, -50);
                await j12_vaso1.TranslateTo(0, -50); 
                await j12_vaso2.TranslateTo(0, -50); 
                await j12_vaso3.TranslateTo(0, -50);
                await j12_vaso2.TranslateTo(-220, -50); 
                await j12_vaso3.TranslateTo(-220, -50); 
                await j12_vaso1.TranslateTo(440, -50);
                await Task.Delay(500);
                await j12_vaso1.TranslateTo(500, 0); 
                await j12_vaso2.TranslateTo(0, 0); 
                await j12_vaso3.TranslateTo(-500, 0);
            }
             
        }

    }
}
