﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorldMiniGamesV1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUp 
    {
        

        //Constructor POPUP con Parametro TXTPUNTOS
        public PopUp(String TxtPuntos, INavigation nav)
        {
            InitializeComponent();
            BindingContext = new NasPageViewModel(TxtPuntos, nav);
        }

        public PopUp(String TxtPuntos, INavigation nav, String Ninja)
        {
            InitializeComponent();
            BindingContext = new NinjasPageViewModel2(TxtPuntos, nav);
        }

        public PopUp(String TxtPuntos, INavigation nav, int Topos)
        {
            InitializeComponent();
            BindingContext = new ToposPageViewModel(TxtPuntos, nav);
        }

        public PopUp(String TxtPuntos, INavigation nav, double Egypt)
        {
            InitializeComponent();
            BindingContext = new EgyptPageViewModel(TxtPuntos, nav);
        }

        public PopUp(String TxtPuntos,INavigation nav, int Suma, int suma2)
        {
            InitializeComponent();
            BindingContext = new SumaPageViewModel(TxtPuntos,nav,this);
        }

        public PopUp(J12_Numero j12_num, String TxtPuntos, INavigation nav, float Vasos)
        {
            InitializeComponent();
            BindingContext = new VasosPageViewModel(j12_num, TxtPuntos, nav);
        }

        public PopUp(String TxtPuntos, INavigation nav, float Toe)
        {
            InitializeComponent();
            BindingContext = new TicToeViewModel(TxtPuntos, nav);
        }




    }
}