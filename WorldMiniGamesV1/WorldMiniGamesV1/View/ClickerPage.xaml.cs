﻿using WorldMiniGamesV1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorldMiniGamesV1.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class ClickerPage : ContentPage
    {
        public ClickerPage()
        {

            InitializeComponent();
            BindingContext = new ClickerPageViewModel(0,0,Navigation);
            

        }

       //MainPageViewModel prueba = new MainPageViewModel();
    }
}