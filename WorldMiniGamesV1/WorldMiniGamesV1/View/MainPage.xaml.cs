﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldMiniGamesV1.View;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainPageViewModel("", Navigation);
        }

        private void Ranking(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Ranking());
        }
    }
}
