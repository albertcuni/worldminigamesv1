﻿using System.ComponentModel;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class TicToePage : ContentPage
    {
        public TicToePage()
        {
            InitializeComponent();
            BindingContext = new TicToeViewModel("", Navigation);
        }
    }
}
