﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class NasPage : ContentPage
    {
        //Constructor MainPage Bindeado al MainPageViewModel


        public NasPage()
        {
            InitializeComponent();
            BindingContext = new NasPageViewModel("",Navigation);
        }

        //Animaciones de CLICK EN BOTONES
        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await btn_azul.ScaleTo(0.9, 50);
            await Task.Delay(200);
            await btn_azul.ScaleTo(1, 50);
        }

        private async void TapGestureRecognizer_Tapped_1(object sender, EventArgs e)
        {
            await btn_blanco.ScaleTo(0.9, 50);
            await Task.Delay(200);
            await btn_blanco.ScaleTo(1, 50);
        }

        private async void TapGestureRecognizer_Tapped_2(object sender, EventArgs e)
        {
            await btn_rojo.ScaleTo(0.9, 50);
            await Task.Delay(200);
            await btn_rojo.ScaleTo(1, 50);
        }

        private async void TapGestureRecognizer_Tapped_3(object sender, EventArgs e)
        {
            await btn_start.ScaleTo(0.9, 50);
            await Task.Delay(200);
            await btn_start.ScaleTo(1, 50);
        }
    }
}
