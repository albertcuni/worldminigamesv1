﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class NinjasPage2 : ContentPage
    {
        public NinjasPage2()
        {
            InitializeComponent();
            BindingContext = new NinjasPageViewModel2("", Navigation);
        
        }

       

        private async void AnimacionShuriken(object sender, EventArgs e)
        {
            await estrella.RotateTo(20, 7);
            await estrella.TranslateTo(0, -15,7);
            await estrella.RotateTo(40, 20);
            await estrella.TranslateTo(0, -30, 7);
            await estrella.RotateTo(60, 20);
            await estrella.TranslateTo(0, -45, 7);
            await estrella.RotateTo(80, 20);
            await estrella.TranslateTo(0, -60, 7);
            await estrella.RotateTo(100, 20);
            await estrella.TranslateTo(0, -75, 7);
            await estrella.RotateTo(120, 20);
            await estrella.TranslateTo(0, -90, 7);
            await estrella.RotateTo(140, 20);
            await estrella.TranslateTo(0, -105, 7);
            await estrella.RotateTo(160, 20);
            await estrella.TranslateTo(0, -120, 7);
            await estrella.RotateTo(180, 20);
            await estrella.TranslateTo(0, -135, 7);
            await estrella.RotateTo(200, 20);
            await estrella.TranslateTo(0, -150, 7);
            estrella.Opacity = 0;
            await estrella.TranslateTo(0,0);
            await estrella.FadeTo(4000);
        }
        private async void AnimacionGalleta(object sender, EventArgs e)
        {
            await galleta.ScaleTo(1.5, 50);
            await galleta.ScaleTo(1, 50);

        }
        private async void AnimacionBonus(object sender, EventArgs e)
        {
            await dragondorado.ScaleTo(0.70, 150);
            await dragondorado.ScaleTo(1, 150);
        }
   
    }
}
