﻿using WorldMiniGamesV1.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldMiniGamesV1.ViewModel;
using Xamarin.Forms;

namespace WorldMiniGamesV1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class EgyptPage : ContentPage
    {
        public EgyptPage()
        {
            InitializeComponent();
            BindingContext = new EgyptPageViewModel("", Navigation);
        
        }
       async void  J3_AnimacionB(Object sender, EventArgs e)
        {
            J3_burbuja.Source = "J3_img/J3_burbuja1.png";
            await Task.Delay(1000);
            J3_burbuja.Source = "J3_img/J3_burbuja.png";
        }

    }
}
